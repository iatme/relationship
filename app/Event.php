<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentTaggable\Taggable;
use Cviebrock\EloquentSluggable\Sluggable;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\File;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cog\Contracts\Love\Likeable\Models\Likeable as LikeableContract;
use Cog\Laravel\Love\Likeable\Models\Traits\Likeable;

class Event extends Model implements HasMedia, LikeableContract
{

    use Taggable, HasMediaTrait, SoftDeletes, Sluggable, Likeable;


    public function registerMediaConversions(Media $media = null) {
      $this->addMediaConversion('small_thumb')
          ->width(150)
          ->height(150)
          ->nonQueued();

      $this->addMediaConversion('medium_thumb')
          ->width(450)
          ->nonQueued();

      $this->addMediaConversion('large_thumb')
          ->width(1200);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'clean_title',
                'maxLength' => 70
            ]
        ];
    }

    /*
     * Mutator clean_title to remove stop words before generate slug.
     *
     * @return string
     */
    public function getCleanTitleAttribute($value) {
        $badWords = ['w/','a','about','above','after','again','all','am','an','and','any','are','as','at','be','because','been','before','being','below','both','but','by','could','did','do','does','doing','down','during','each','few','for','from','further','had','has','have','having','he','he\'d','he\'ll','he\'s','her','here','here\'s','hers','herself','him','himself','his','how','how\'s','I','I\'d','I\'ll','I\'m','I\'ve','if','in','into','is','it','it\'s','its','itself','let\'s','me','more','most','my','myself','nor','of','on','once','only','or','other','ought','our','ours','ourselves','out','over','own','same','she','she\'d','she\'ll','she\'s','should','so','some','such','than','that','that\'s','the','their','theirs','them','themselves','then','there','there\'s','these','they','they\'d','they\'ll','they\'re','they\'ve','this','those','through','to','too','until','up','very','was','we','we\'d','we\'ll','we\'re','we\'ve','were','what','what\'s','when','when\'s','where','where\'s','which','while','who','who\'s','whom','why','why\'s','with','would','you','you\'d','you\'ll','you\'re','you\'ve','your','yours','yourself','yourselves'];

        // convert each word into a regex
        array_walk($badWords, function(&$word) {
            $word = '/\b' . preg_quote($word, '/') . '\b/i';
        });

        // strip the bad words from the title
        $value = preg_replace($badWords, '', $this->title);

        // return the cleaned title
        return $value;
    }

    /*
     * Only allow certain files in a collection
     *
     * @return void
     */
    public function registerMediaCollections()
    {
      $this
        ->addMediaCollection('images')
        ->acceptsFile(function (File $file) {
            return 
              $file->mimeType === 'image/jpeg' 
              || $file->mimeType === 'image/png' 
              || $file->mimeType === 'image/gif';
        });

      $this
        ->addMediaCollection('documents')
        ->acceptsFile(function (File $file) {
            return 
              $file->mimeType === 'application/pdf' 

              || $file->mimeType === 'application/msword' 
              || $file->mimeType === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' 

              || $file->mimeType === 'application/vnd.ms-powerpoint' 
              || $file->mimeType === 'application/vnd.openxmlformats-officedocument.presentationml.presentation'

              || $file->mimeType === 'application/vnd.ms-excel'
              || $file->mimeType === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        });
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'start_time', 'end_time', 'status', 'is_private', 'password', 'event_types_id'];

    public function topics()
    {
        return $this->hasMany('App\Topic');
    }

    public function eventType(){
      return $this->belongsToMany('App\EventType');
    }
}
