<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{


	public $timestamps = false;
    protected $fillable = ['conversation_id', 'sender_id', 'body', 'is_seen', 'created_at', 'updated_at', 'delete_at'];

    public function sender()
    {
    	return $this->belongsTo('App\User');
    }

    public function conversation()
    {
    	return $this->belongsTo('App\Conversation');
    }
   
}
