<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;
use Carbon\Carbon;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Register the routes necessary to issue access tokens and revoke access tokens, clients, and personal access tokens
        Passport::routes();
        Passport::tokensExpireIn(Carbon::now()->addHours(12));
        Passport::refreshTokensExpireIn(Carbon::now()->addMonths(1));
    }
}
