<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    protected $fillable = ['type_name', 'created_at', 'updated_at'];

    public function event(){
    	return $this->belongsToMany('App\Event');
    }
}
