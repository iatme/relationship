<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
	public $timestamps = false;
    protected $fillable = ['title', 'user1_id', 'user2_id', 'created_at'/*, 'user1_deleted', 'user2_deleted', 'lastest_message_excerpt', 'lastest_message_time', 'lastest_message_seen', 'lastest_message_sender_id'*/, 'is_accepted'];
    //public function users()
    //{
    	//return $this->belongsToMany('App\User','conversations_members');
    //}
    public function user1()
    {
    	return $this->belongsTo('App\User');
    }

    public function user2()
    {
    	return $this->belongsTo('App\User');
    }

     public function lastest_message_sender()
    {
    	return $this->belongsTo('App\User');
    }
}
