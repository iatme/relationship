<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Notifications\SignupActivate;
use Zend\Diactoros\ServerRequest;
use Zend\Diactoros\Response;
use League\OAuth2\Server\AuthorizationServer;

class AuthController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed'
        ]);

        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'activation_token' => str_random(60)
        ]);
        $user->save();

        $user->notify(new SignupActivate($user));

        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }

    /**
     * Activate new signing up user
     * 
     * @param   [string] token
     * @return  [string] message
     */
    public function signupActivate($token)
    {
        $user = User::where('activation_token', $token)->first();

        if (!$user)
        {
            return response()->json(['message' => 'This activation token is invalid.'], 404);
        }

        $user->active = true;
        $user->activation_token = '';
        $user->save();

        return $user;
    }
  
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);
        $credentials['active'] = 1;
        $credentials['deleted_at'] = null;

        if(!Auth::attempt($credentials, $request->remember_me))
        {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }

        $token = json_decode(resolve(AuthorizationServer::class)->respondToAccessTokenRequest((new ServerRequest)->withParsedBody([
            'grant_type' => 'password',
            'client_id' => 2,
            'client_secret' => 'w8NgtqKphMqMFQOFxIufwHqg6LslqKizCpZcvlNK',
            'username' => $credentials['email'],
            'password' => $credentials['password'],
            'scope' => ''
        ]), new Response)->getBody()->__toString(), true);
        return $token;
    }
  
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
  
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    /**
     * Refresh existence access token
     * 
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function refresh(Request $request)
    {
        $token = json_decode(resolve(AuthorizationServer::class)->respondToAccessTokenRequest((new ServerRequest)->withParsedBody([
            'grant_type' => 'refresh_token',
            'client_id' => 2,
            'client_secret' => 'w8NgtqKphMqMFQOFxIufwHqg6LslqKizCpZcvlNK',
            'refresh_token' => $request->refresh_token,
            'scope' => ''
        ]), new Response)->getBody()->__toString(), true);
        return $token;
    }
}
