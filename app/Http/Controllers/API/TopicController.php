<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Topic;
use App\Comment;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TopicController extends Controller
{
    public function index()
    {
        return Topic::all();
    }

    public function show($id)
    {
        $topic = Topic::find($id);
        return $topic ? $topic : response()->json(['message' => 'Topic not found!'], 404);
    }

    public function store(Request $request)
    {
        $request->validate([
            'event_id' => 'required|integer|exists:events,id',
            'title' => 'required|string',
            'body' => 'required|string',
            'is_pinned' => 'required|boolean',
            'comment_count' => 'integer'
        ]);

        $topic = Topic::create($request->all());
        return response()->json($topic, 201);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'event_id' => 'integer|exists:events,id',
            'title' => 'string',
            'body' => 'string',
            'is_pinned' => 'boolean',
            'comment_count' => 'integer'
        ]);
        try
        {
            $topic = Topic::findOrFail($id);
            $topic->update($request->all());
            return $topic;
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['message' => 'Topic not found!'], 404);
        }
    }

    public function destroy($id)
    {
        try
        {
            $topic = Topic::findOrFail($id);
            $topic->delete();
            return response()->json(['message' => 'Topic is deleted successfully!'], 200);
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['message' => 'Topic not found!'], 404);
        }
    }

    public function showComments($topic_id)
    {
        try
        {
            $topic = Topic::findOrFail($topic_id);
            return $topic->comments;
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['message' => 'Topic not found!'], 404);
        }
    }

    public function storeComment($topic_id, Request $request)
    {
        $request->validate([
            'comment' => 'required|string'
        ]);
        try
        {
            $topic = Topic::findOrFail($topic_id);
            $commentData = $request->all();
            $commentData['commentator_id'] = $request->user()->id;
            $commentData['topic_id'] = $topic->id;
            $comment = Comment::create($commentData);
            return response()->json($comment, 201);
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['message' => 'Topic not found!'], 404);
        }
    }
}
