<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Conversation;
use Illuminate\Support\Facades\Auth;

class ConversationController extends Controller
{
    public function index()
    {
        // $conversation = Conversation::all();
        // return response()->json($conversation);
        $user = Auth::guard("api")->user();
        if (!$user)
        {
            return response()->json(['message' => 'Missing access_token in the request'], 401);
        }
        $conversation = Conversation::where('user1_id', '=', $user->id)->orWhere('user2_id', '=', $user->id)->get();
        return $conversation ? response()->json($conversation) : response()->json(['message' => 'Conversation not found!'], 404);
    }

    public function show($id)
    {
        $conversation = Conversation::find($id);
        return $conversation ? response()->json($conversation) : response()->json(['message' => 'Conversation not found!'], 404);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'string|nullable',
            'user1_id' => 'required|integer|exists:users,id',
            'user2_id' => 'required|integer|exists:users,id',
            'created_at' => 'date|nullable',
            // 'user1_deleted' => 'boolean|nullable',
            // 'user2_deleted' => 'boolean|nullable',
            // 'lastest_message_excerpt' => 'string|nullable',
            // 'lastest_message_time' => 'date|nullable',
            // 'lastest_message_seen' => 'boolean|nullable',
            // 'lastest_message_sender_id' =>'required|integer|exists:Users,id',
            'is_accepted' => 'boolean|nullable',
        ]);
        // if (is_null($request->lastest_message_seen)) {
        //     $request->lastest_message_seen = false;
        // }
        // if (is_null($request->user1_deleted)) {
        //     $request->user1_deleted = false;
        // }
        // if (is_null($request->user2_deleted)) {
        //     $request->user2_deleted = false;
        // }
        if (is_null($request->is_accepted)) {
            $request->is_accepted = false;
        }
        $conversation = new Conversation([
            'title' => $request->title,
            'user1_id' => $request->user1_id,
            'user2_id' => $request->user2_id,
            'created_at' => $request->created_at,
            //'user1_deleted' => $request->user1_deleted,
            //'user2_deleted' => $request->user2_deleted,
            // 'lastest_message_excerpt' => $request->lastest_message_excerpt,
            // 'lastest_message_time' => $request->lastest_message_time,
            // 'lastest_message_seen' => $request->lastest_message_seen,
            // 'lastest_message_sender_id' => $request->lastest_message_sender_id,
            'is_accepted' => $request->is_accepted,
        ]);
        $conversation->save();
        return response()->json([
            'message' => 'Successfully created conversation! with ID = ' . $conversation->id
        ], 201);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'string|nullable',
            'user1_id' => 'required|integer|exists:users,id',
            'user2_id' => 'required|integer|exists:users,id',
            'created_at' => 'nullable|date',
            // 'user1_deleted' => 'boolean|nullable',
            // 'user2_deleted' => 'boolean|nullable',
            // 'lastest_message_excerpt' => 'string|nullable',
            // 'lastest_message_time' => 'date|nullable',
            // 'lastest_message_seen' => 'boolean|nullable',
            // 'lastest_message_sender_id' =>'required|integer|exists:Users,id',
            'is_accepted' => 'boolean|nullable',
        ]);
        $conversation = Conversation::find($id);
        if ($conversation)
        {
            $conversation->update($request->all());
            return response()->json($conversation, 200);
        }
        else
        {
            return response()->json(['message' => 'Can not find the conversation with ID = ' .$id ], 400);
        }
    }

    public function destroy(Request $request, $id)
    {
        $conversation = Conversation::find($id);
        if ($conversation)
        {
            $conversation->delete();
            return response()->json(['message' => 'Successfully delete conversation!'], 200);
        }
        else
        {
            return response()->json(['message' => 'Can not find the conversation with ID = ' .$id ], 400);
        }
    }
}
