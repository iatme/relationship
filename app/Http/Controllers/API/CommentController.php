<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CommentController extends Controller
{
    private const COMMENT_NOT_FOUND_MESSAGE = "Comment not found!";
    private const NO_PERMISSION_MESSAGE = "You don't have permission to do that!";

    public function index()
    {
        //if (isOrganizer())
        {
            return Comment::all();
        }
        //return response()->json(['message' => self::NO_PERMISSION_MESSAGE], 403);
    }

    public function show($id)
    {
        $comment = Comment::find($id);
        return $comment ? $comment : response()->json(['message' => self::COMMENT_NOT_FOUND_MESSAGE], 404);
    }

    // public function store(Request $request)
    // {
    //     $request->validate([
    //         'commentator_id' => 'required|integer|exists:users,id',
    //         'comment' => 'required|string',
    //         'topic_id' => 'required|integer|exists:topics,id'
    //     ]);
    //     $comment = Comment::create($request->all());
    //     return response()->json($comment, 201);
    // }

    public function update(Request $request, $id)
    {
        $request->validate([
            'commentator_id' => 'integer|exists:users,id',
            'comment' => 'string',
            'topic_id' => 'integer|exists:topics,id'
        ]);
        try
        {
            $comment = Comment::findOrFail($id);
            if ($comment->commentator_id === $request->user()->id /*|| isOrganizer()*/)
            {
                $commentData = $request->all();
                if ($comment->commentator_id === $request->user()->id)
                {
                    $commentData = request(['comment']);
                }
                $comment->update($commentData);
                return $comment;
            }
            return response()->json(['message' => self::NO_PERMISSION_MESSAGE], 403);
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['message' => self::COMMENT_NOT_FOUND_MESSAGE], 404);
        }
    }

    public function destroy(Request $request, $id)
    {
        try
        {
            $comment = Comment::findOrFail($id);
            if ($comment->commentator_id === $request->user()->id /*|| isOrganizer()*/)
            {
                $comment->delete();
                return response()->json(['message' => 'Comment is deleted successfully!'], 200);
            }
            return response()->json(['message' => self::NO_PERMISSION_MESSAGE], 403);
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json(['message' => self::COMMENT_NOT_FOUND_MESSAGE], 404);
        }
    }
}
