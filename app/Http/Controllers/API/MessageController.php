<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;

class MessageController extends Controller
{
    public function index()
    {
        $message = Message::all();
        return response()->json($message);
    }

    public function show($id)
    {
        $message = Message::find($id);
        return $message ? response()->json($message) : response()->json(['message' => 'Message not found!'], 404);
    }

    public function store(Request $request)
    {
        $request->validate([
            'conversation_id' => 'required|integer|exists:conversations,id',
            'sender_id' => 'required|integer|exists:users,id',
            'body' => 'required|string',
            'is_seen' => 'boolean|nullable',
            'deleted_at' => 'date|nullable',
        ]);
        if (is_null($request->is_seen)) {
            $request->is_seen = false;
        }
        $message = new Message([
            'conversation_id' => $request->conversation_id,
            'sender_id' => $request->sender_id,
            'body' => $request->body,
            'is_seen' => $request->is_seen,
            'deleted_at' => $request->deleted_at,
        ]);
        $message->save();
        return response()->json([
            'message' => 'Successfully created message with ID = ' . $message->id
        ], 201);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'conversation_id' => 'required|integer|exists:conversations,id',
            'body' => 'required|string',
            'sender_id' => 'required|integer|exists:users,id',
            'is_seen' => 'boolean|nullable',
            'deleted_at' => 'date|nullable',
        ]);
        $message = Message::find($id);
        if ($message)
        {
            $message->update($request->all());
            return response()->json($message, 200);
        }
        else
        {
            return response()->json(['message' => 'Can not find the message with ID = ' .$id ], 400);
        }
    }

    public function destroy(Request $request, $id)
    {
        $message = Message::find($id);
        if ($message)
        {
            $message->delete();
            return response()->json(['message' => 'Successfully delete message!'], 200);
        }
        else
        {
            return response()->json(['message' => 'Can not find the message with ID = ' .$id ], 400);
        }
    }

    //xuan.maithanh added
    public function showAllMgFromConv(Request $request, $conversation_id)
    {        
        $message = Message::where('conversation_id', '=', $conversation_id)->get();
        return count($message) > 0 ? response()->json($message) : response()->json(['message' => 'There is no message on the conversion = '. $request->conversation_id], 404);
    }

}
