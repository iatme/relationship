<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Event;
use App\User;
use App\EventEventType;
use App\EventType;
use Spatie\MediaLibrary\Models\Media;
use App\Http\Requests\StoreEventRequest;
use App\Http\Requests\UpdateEventRequest;
use App\Http\Requests\UpdateImageMetaRequest;
use App\Http\Requests\UploadImagesToEvent;
use App\Http\Controllers\Controller;
use App\Http\Resources\Event as EventResource;
use App\Http\Resources\EventCollection;
use Illuminate\Support\Facades\Auth;
use League\OAuth2\Server\AuthorizationServer;

class EventController extends Controller
{

    public function index(Request $request)
    {
        if ($request->query('tags')) {
            $event = Event::withAnyTags($request->query('tags'))->get();
        } else {
            $event = Event::all();
        }

        return response()->json(['data'=>$event]);
    }

    public function show($id)
    {
        $event = Event::find($id);
        if (!$event) return response()->json(['message' => 'Not found event('.$id.').'], 404);
        
        $mediaItems = $event->getMedia(); // get both images and documents

        return response()->json(['data'=>$event]);
    }

    public function store(StoreEventRequest $request)
    {
        $event = Event::create($request->all());
        
        if (!empty($request->tags)) $event->tag($request->tags);

        $this->uploadAttachments($request, $event);

        return response()->json([
            'message' => 'Event created successfully.'
        ], 201);
    }
    
    public function update(UpdateEventRequest $request, $id)
    {
        $event = Event::find($id);
        if (!$event) {
            return response()->json(['message' => 'Not found event('.$id.').' ], 400);
        }

        $event->update($request->all());

        if (!empty($request->tags)) $event->retag($request->tags);

        $this->uploadAttachments($request, $event);
        
        return response()->json(['data'=>$event]);
    }

    public function updateFeaturedImage($event)
    {
        $event->featured_image = $event->getFirstMediaUrl('images');
        $event->save();
    }

    public function destroy(Request $request, $id)
    {
        $event = Event::find($id);
        if (!$event) {
            return response()->json(['message' => 'Not found event('.$id.').' ], 400);
        }
        
        // remove tags
        $event->detag();

        // remove all associated files & event
        $event->delete();

        return response()->json(['message' => 'Event('.$id.') deleted successfully.'], 200);
    }

    public function uploadAttachments($request, $event)
    {
        if ($request->hasFile('images')) {

            $fileAdders = collect( 
                $event->addMultipleMediaFromRequest(["images"])
                // $event->addAllMediaFromRequest()
            );
            $fileAdders->each(function($fileAdder){
                $fileAdder = is_array($fileAdder) ? $fileAdder : [$fileAdder];
                foreach ($fileAdder as $item) {
                    $item
                        ->sanitizingFileName(function($fileName) {
                            return strtolower(str_replace(['#', '/', '\\', ' '], '-', $fileName));
                        })
                        ->toMediaCollection('images');
                }
            });

            // Set feature image is the 1st image
            if (empty($event->featured_image)) {
                $this->updateFeaturedImage($event);
            }
        }

        if ($request->hasFile('documents')) {
            $fileAdders = collect( 
                $event->addMultipleMediaFromRequest(["documents"])
            );
            $fileAdders->each(function($fileAdder){
                $fileAdder = is_array($fileAdder) ? $fileAdder : [$fileAdder];
                foreach ($fileAdder as $item) {
                    $item
                        ->sanitizingFileName(function($fileName) {
                            return strtolower(str_replace(['#', '/', '\\', ' '], '-', $fileName));
                        })
                        ->toMediaCollection('documents');
                }
            });

        }
    }

    public function listImages(Request $request, $event_id)
    {
        $event = Event::find($event_id);
        if (!$event) {
            return response()->json(['message' => 'Not found event('.$event_id.').' ]);
        }
        
        $mediaItems = $event->getMedia('images');

        return response()->json(['data'=>$mediaItems]);
    }

    public function listDocuments(Request $request, $event_id)
    {
        $event = Event::find($event_id);
        if (!$event) {
            return response()->json(['message' => 'Not found event('.$event_id.').' ]);
        }
        
        $mediaItems = $event->getMedia('documents');

        return response()->json(['data'=>$mediaItems]);
    }

    public function listMedia(Request $request, $event_id)
    {
        $event = Event::find($event_id);
        if (!$event) {
            return response()->json(['message' => 'Not found event('.$event_id.').' ]);
        }
        
        $mediaItems = $event->getMedia();

        return response()->json(['data'=>$mediaItems]);
    }

    public function uploadMedia(UploadImagesToEvent $request, $event_id)
    {
        $event = Event::find($event_id);
        if (!$event) {
            return response()->json(['message' => 'Not found event('.$event_id.').' ]);
        }
        
        $result = $this->uploadAttachments($request, $event);

        return response()->json($event);
        
    }

    public function changeImageAttributes(UpdateImageMetaRequest $request, $event_id, $media_id)
    {
        $event = Event::find($event_id);
        if (!$event) {
            return response()->json(['message' => 'Not found event('.$event_id.').' ]);
        }
        
        if (!$request->img_title && !$request->img_alt) {
            return response()->json(['message' => 'Bad request (Missing attributes "img_title" and "img_alt").' ], 400);
        }

        $mediaItem = Media::find($media_id);
        if (!$mediaItem) {
            return response()->json(['message' => 'Not found image('.$media_id.').' ], 404);
        }

        if ($request->img_title) {
            $mediaItem->setCustomProperty('title',$request->img_title);
        }
        if ($request->img_alt) {
            $mediaItem->setCustomProperty('alt',$request->img_alt);
        }

        $mediaItem->save();

        return response()->json(['message' => 'Image('.$media_id.') \'s attributes updated successfully.' ], 200);
        
    }

    public function changeMediaOrder(Request $request, $event_id, $media_id)
    {
        if (!$request->new_position) {
            return response()->json(['message' => 'Bad request (Missing attributes "new_position").' ], 400);
        }

        $event = Event::find($event_id);
        if (!$event) {
            return response()->json(['message' => 'Not found event('.$event_id.').' ], 404);
        }

        $mediaItem = Media::find($media_id);
        if (!$mediaItem) {
            return response()->json(['message' => 'Not found media('.$media_id.').' ], 404);
        }

        $mediaItem->order_column = intval($request->new_position);
        $mediaItem->save();

        // reset featured image for event
        $this->updateFeaturedImage($event);

        return response()->json(['message' => 'Media('.$media_id.') \'s position hanged successfully.' ], 200);
    }

    public function reorderMedia(Request $request, $event_id)
    {
        $event = Event::find($event_id);
        if (!$event) {
            return response()->json(['message' => 'Not found event('.$event_id.').' ], 404);
        }

        if (!$request->new_positions) {
            return response()->json(['message' => 'Bad request (Missing attributes "new_positions").' ], 400);
        }

        $mediaIDs = explode(',',$request->new_positions);

        $mediaItems = Media::whereIn('id', $mediaIDs)->get();

        if ($mediaItems->isEmpty()) {
            return response()->json(['message' => 'No media found.' ], 404);
        }

        if ($mediaItems->count() != count($mediaIDs)) {
            return response()->json(['message' => 'There is invalid media ID(s).' ], 400);
        }

        foreach($mediaItems AS $item) {
            if ($item->model_id != $event_id) {
                return response()->json(['message' => 'Media('.$item->id.') not belong to the event('.$event_id.').'], 400);
            }
        }

        Media::setNewOrder($mediaIDs);

        return response()->json(['message' => 'Media positions re-ordered successfully.' ], 200);
        
    }

    public function deleteMedia(Request $request, $event_id, $media_id)
    {
        $event = Event::find($event_id);
        if (!$event) {
            return response()->json(['message' => 'Not found event('.$event_id.').' ], 404);
        }

        $mediaItem = Media::find($media_id);

        if (!$mediaItem) {
            return response()->json(['message' => 'Not found media('.$media_id.').' ], 404);
        }

        if ($mediaItem->model_id != $event_id) {
            return response()->json(['message' => 'Media('.$mediaItem->id.') not belong to the event('.$event_id.').'], 400);
        }

        $mediaItem->delete();

        return response()->json(['message' => 'Media('.$media_id.') deleted successfully.' ], 200);
        
    }

    //xuan.maithanh added some functions for event liked
    public function getListEventLiked(Request $request)
    {
        $user = Auth::guard("api")->user();
        // $user = $request->user();
        if (!$user)
        {
            return response()->json(['message' => 'Missing access_token in the request' ], 401);
        }
        $event = Event::whereLikedBy($user->id)->get();
        return response()->json(['data'=>$event]);
    }

    public function setEventLiked(Request $request, $event_id)
    {       
        $event = Event::find($event_id);
        if (!$event) {
            return response()->json(['message' => 'Not found event('.$event_id.').' ], 404);
        }

        $user = Auth::guard("api")->user();
        // $user = $request->user();
        if (!$user)
        {
            return response()->json(['message' => 'Missing access_token in the request' ], 401);
        }
        $user->like($event);
        $event->likeBy($user->id);
        
        return response()->json(['message' => 'Liked event ('.$event_id.') succesfully' ], 200);
    }

    public function deleteEventLiked(Request $request, $event_id)
    {
        $event = Event::find($event_id);
        if (!$event) {
            return response()->json(['message' => 'Not found event('.$event_id.').' ], 404);
        }

        $user = Auth::guard("api")->user();
        // $user = $request->user();
        if (!$user)
        {
            return response()->json(['message' => 'Missing access_token in the request' ], 401);
        }
        $user->unlike($event);
        $event->unlikeBy($user->id);
    
        return response()->json(['message' => 'Unliked event ('.$event_id.') succesfully' ], 200);
    }

    public function getEvents($event_id){
        $event = Event::find($event_id);
        if(!$event){
            return response()->json(['message' => 'Not found event('.$event_id.').'], 404);
        }
        return response()->json([ "data" => $event,$event->eventType]);
    }

    public function getAll(){
        $event = Event::all();
        $a = "";
        foreach ($event as $value) {
            $a = $value->eventType;
        }
        if(!$event){
            return response()->json(['message' => 'Not found event('.$event_id.').'], 404);
        }
        return response()->json(['data' => $event]);
    }
    public function getType($type_id){
        $event = EventEventType::where('event_type_id',$type_id)->get();
        foreach ($event as $value) {
            $a = $value->event;
        }
        foreach ($event as $value) {
            $a = $value->eventtype;
        }
        if($event->count() == 0){
            return response()->json(['message' => 'Not found event type('.$type_id.').'], 404);
        }
        return response()->json(['data' => $event]);
    }
    
}
