<?php

namespace App\Helpers;

use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\PathGenerator\PathGenerator;


class MediaCustomPathGenerator implements PathGenerator
{
    public function getModelPath(Media $media) : string
    {
        $secret = config('app.secret') ?? 'shhhhh';
        return (new $media->model_type)->getTable().'/'.$media->model_id.'/'.$media->collection_name.'/';//.base64_encode($media->model_id.'.'.sha1($secret)).'/';
    }

    public function getPath(Media $media) : string
    {
        return $this->getModelPath($media).base64_encode($media->id).'/';
    }

    public function getPathForConversions(Media $media) : string
    {
        return $this->getPath($media).'sizes/';
    }
    
    public function getPathForResponsiveImages(Media $media): string
    {
        return $this->getPath($media).'/responsive/';
    }
}