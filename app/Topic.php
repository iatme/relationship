<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $fillable = ['event_id', 'title', 'body', 'is_pinned', 'comment_count'];

    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
