<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventEventType extends Model
{
	protected $table = 'event_event_type';
    protected $fillable = ['event_id', 'event_type_id'];

    public function event(){
    	return $this->belongsTo('App\Event');
    }
    public function eventtype(){
    	return $this->belongsTo('App\EventType');
    }
}
