Do please below step after you checkout the repo
1. Copy .env.example to .env
2. Config .env to be fit with the environment that using for the project (prefer to use local database with mysql & local server at this time)
3. In the root folder of the project, call: composer update, to add the dependency libs
4. Generate the key by command: php artisan key:generate
5. Create the database and migrate: php artisan migrate
5. Generate encryption keys and clients for passport that is necessary to generate tokens: php artisan passport:install
6. Run the server by "command: php artisan serve", and test with the browser.

------------------------------------------------------------------------------------------------------------------
SETUP DATABASE IN LOCAL PC
run mysql (command : mysql -u root -p, enter the password), create the database (command : create database Munifyevent)
//in case you want to define the database is Munifyevent
Using phpAdmin (install and access from browser for local : http://localhost/phpmyadmin)
------------------------------------------------------------------------------------------------------------------
API
A document : https://www.toptal.com/laravel/restful-laravel-api-tutorial
php artisan db:seed --> to seed all mock data.
phpunit --> to test all test cases. Add the result screen trello card for reviewer to see the result.
------------------------------------------------------------------------------------------------------------------
WORKING ON STAGING/PRODUCTION ENVIRONMENT
su -s /bin/bash nobody -c 'composer update' : to safe run composer update
su -s /bin/bash nginx -c 'php -d memory_limit=-1 /usr/local/bin/composer update'
php -d memory_limit=-1 /usr/local/bin/composer update

------------------------------------------------------------------------------------------------------------------
Setting mailbox for Laravel (using gmail)
1. Login your gmail account on google
2. Setup 2 step verification with Authenticator app for your gmail account
3. Open URL https://security.google.com/settings/security/apppasswords and sign in with your gmail account
4. Generate a new app password for your gmail account
5. Open .env file
6. Config below lines
        MAIL_DRIVER=smtp
        MAIL_HOST=smtp.gmail.com
        MAIL_PORT=465
        MAIL_USERNAME=<your_google_email>@gmail.com
        MAIL_PASSWORD=<your_app_password_in_step_4>
        MAIL_ENCRYPTION=SSL
7. Open file \vendor\swiftmailer\swiftmailer\lib\classes\Swift\Transport\StreamBuffer.php in project
8. Find function establishSocketConnection()
9. Add 2 below lines before function calling of stream_context_create()
        $options['ssl']['verify_peer'] = FALSE;
        $options['ssl']['verify_peer_name'] = FALSE;
------------------------------------------------------------------------------------------------------------------
SETUP WEB FRONTEND IF MODIFY
1. INSTALL APP DEPENDENCIES by run command: npm install
3. BUILD APP for testing by run command: npm run dev
4. Run the server by command : php artisan serve and test with the browser.
