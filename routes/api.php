<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {

  Route::post('login', 'AuthController@login');
  Route::post('signup', 'AuthController@signup');
  Route::get('signup/activate/{token}', 'AuthController@signupActivate');
  Route::post('recovery', 'Auth\\PasswordResetController@create');
  Route::get('find/{token}', 'Auth\\PasswordResetController@find');
  Route::post('reset', 'Auth\\PasswordResetController@reset');
  Route::post('refresh', 'AuthController@refresh');

  Route::group([
    'middleware' => 'auth:api'
  ], function() {
      Route::get('logout', 'AuthController@logout');
      Route::get('me', 'AuthController@user');
  });

  // Route::middleware('auth:api')->get('/user', function (Request $request) {
  //     return $request->user();
  // });

});

Route::apiResource('events', 'API\\EventController');

Route::get('events/{event_id}/images', 'API\\EventController@listImages');
Route::get('events/{event_id}/documents', 'API\\EventController@listDocuments');
Route::get('events/{event_id}/media', 'API\\EventController@listMedia');
Route::get('liked-events', 'API\\EventController@getListEventLiked'); //xuan.maithanh added - get all event liked from user

Route::post('events/{event_id}/media', 'API\\EventController@uploadMedia');
Route::post('liked-events/{event_id}', 'API\\EventController@setEventLiked'); //xuan.maithanh added - set like an event

Route::put('events/{event_id}/images/{media_id}', 'API\\EventController@changeImageAttributes');
Route::put('events/{event_id}/media/{media_id}', 'API\\EventController@changeMediaOrder');
Route::put('events/{event_id}/reorder-media', 'API\\EventController@reorderMedia');


Route::delete('events/{event_id}/media/{media_id}', 'API\\EventController@deleteMedia');
Route::delete('liked-events/{event_id}', 'API\\EventController@deleteEventLiked'); //xuan.maithanh added - unlike an event

Route::apiResource('conversations', 'API\\ConversationController');
Route::apiResource('messages', 'API\\MessageController');
Route::get('conversations/{conversation_id}/messages', 'API\\MessageController@showAllMgFromConv');

Route::group([
  'middleware' => 'auth:api'
], function() {
  Route::apiResource('topics', 'API\\TopicController');
  Route::apiResource('comments', 'API\\CommentController');
  Route::get('topics/{topic_id}/comments', 'API\\TopicController@showComments');
  Route::post('topics/{topic_id}/comments', 'API\\TopicController@storeComment');
});


Route::get('/events-4', 'API\\EventController@getValue');
Route::get('/allEvents', 'API\\EventController@getValues');
Route::get('/event/{event_id}', 'API\\EventController@getMore');

Route::get('event_types/{event_id}/events', 'API\\EventController@getEvents');
Route::get('events', 'API\\EventController@getAll');
Route::get('event_types/{type_id}', 'API\\EventController@getType');
