<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Message;

class MessagesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testsMessagesAreCreatedCorrectly()
    {
        $response = $this->call('POST', '/api/messages/', [
               'conversation_id' => 1, 'sender_id' => 1, 'body' => 'ex body create', 'is_seen' => false
            ]);
        $response->assertStatus(201);
    }

    public function testsMessagesAreUpdatedCorrectly()
    {
        // $this->assertTrue(true);
        $response = $this->call('PUT', '/api/messages/1', [
                 'conversation_id' => 1, 'sender_id' => 1, 'body' => 'ex body update', 'is_seen' => true,
            ]);
        $response->assertStatus(200);

    }

    public function testsMessagesAreDeletedCorrectly()
    {
        //Create an conversations before delete it. Will consider to use Factory later.
        $message = factory(Message::class)->create();
        $response = $this->call('DELETE', '/api/messages/' . $message->id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testMessagesAreListedCorrectly()
    {
        // $this->assertTrue(true);
        $response = $this->get("/api/messages/");
        $response->assertStatus(200);
    }
}
