<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Event;
// use Faker\Factory;

class EventTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testsEventsAreCreatedCorrectly()
    {
        $response = $this->call('POST', '/api/events/', [
                'event_title' => 'ex title', 'event_description' => 'ex des'
            ]);
        $response->assertStatus(201);
    }

    public function testsEventsAreUpdatedCorrectly()
    {
        // $this->assertTrue(true);
        $response = $this->call('PUT', '/api/events/1', [
                'event_title' => 'ex title', 'event_description' => 'ex update'
            ]);
        $response->assertStatus(200);

    }

    public function testsEventsAreDeletedCorrectly()
    {
        //Create an event before delete it. Will consider to use Factory later.
        $this->assertTrue(true);
        $event = new Event([
            'event_title' => 'ex title',
            'event_description' => 'ex create to delete'
        ]);
        $event->save();
    	$response = $this->call('DELETE', '/api/events/' . $event->id);
    	$this->assertEquals(204, $response->getStatusCode());
    }

    public function testEventsAreListedCorrectly()
    {
        // $this->assertTrue(true);
    	$response = $this->get("/api/events/");
        $response->assertStatus(200);
    }

}
