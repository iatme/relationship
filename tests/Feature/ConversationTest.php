<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Conversation;

class ConversationTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testsConversationsAreCreatedCorrectly()
    {
        $response = $this->call('POST', '/api/conversations/', [
               'title' => 'ex title create', 'user1_id' => 1, 'user2_id' => 1, 'lastest_message_excerpt' => 'message created', 'lastest_message_sender_id' => 1
            ]);
        $response->assertStatus(201);
    }

    public function testsConversationsAreUpdatedCorrectly()
    {
        // $this->assertTrue(true);
        $response = $this->call('PUT', '/api/conversations/1', [
                'title' => 'ex title update', 'user1_id' => 1, 'user2_id' => 1, 'lastest_message_sender_id' => 1
            ]);
        $response->assertStatus(200);

    }

    public function testsConversationsAreDeletedCorrectly()
    {
        $conversations = factory(Conversation::class)->create();
        $response = $this->call('DELETE', '/api/conversations/' . $conversations->id);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testConversationsAreListedCorrectly()
    {
        // $this->assertTrue(true);
        $response = $this->get("/api/conversations/");
        $response->assertStatus(200);
    }
}
