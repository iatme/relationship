<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Topic;
use App\Event;

class TopicTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testsTopicsAreCreatedCorrectly()
    {
        $faker = \Faker\Factory::create();
        $payload = [
            'event_id' => Event::all(['id'])->random()->id,
            'title' => $faker->sentence,
            'body' => $faker->paragraph,
            'is_pinned' => $faker->boolean
        ];
        $this->json('POST', '/api/topics', $payload)
            ->assertStatus(201);
    }

    public function testsTopicsAreUpdatedCorrectly()
    {
        $faker = \Faker\Factory::create();
        $topic = factory(Topic::class)->create();
        $payload = [
            'title' => $faker->sentence,
            'body' => $faker->paragraph
        ];
        $this->json('PUT', '/api/topics/' . $topic->id, $payload)
            ->assertStatus(200);
    }

    public function testsTopicsAreDeletedCorrectly()
    {
        $topic = factory(Topic::class)->create();
        $this->json('DELETE', '/api/topics/' . $topic->id)
            ->assertStatus(200);
    }

    public function testsTopicsAreListedCorrectly()
    {
        $topic1 = factory(Topic::class)->create();
        $topic2 = factory(Topic::class)->create();
        $this->json('GET', '/api/topics')
            ->assertStatus(200)
            ->assertJsonStructure(['*' => ['id', 'event_id', 'title', 'body', 'created_at', 'updated_at', 'is_pinned', 'comment_count']]);
    }
}
