<?php

use Illuminate\Database\Seeder;
use App\Message;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Message::count() == 0) {
        	factory(App\Message::class, 20)->make()->each(function ($message) {
                $message->save();
            });
        }
    }
}
