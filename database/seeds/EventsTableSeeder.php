<?php

use Illuminate\Database\Seeder;
use App\Event;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Event::count() == 0) {
            Event::truncate();
            $faker = \Faker\Factory::create();
            for ($i = 0; $i < 50; $i++) {
                Event::create([
                    'event_title' => $faker->sentence,
                    'event_description' => $faker->paragraph,
                ]);
            }
        }
    }
}
