<?php

use Illuminate\Database\Seeder;
use App\Conversation;
use App\User;

class ConversationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Conversation::count() == 0) {
            factory(App\Conversation::class, 20)->make()->each(function ($conversation) {
                $conversation->save();
            });
        }
    }
}
