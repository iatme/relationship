<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::disableForeignKeyConstraints();
        Schema::create('conversations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->integer('user1_id')->unsigned()->index();
            $table->foreign('user1_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('user2_id')->unsigned()->index();
            $table->foreign('user2_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamp('created_at')->useCurrent();
            // $table->integer('user1_deleted_message_id')->unsigned()->index();
            // $table->foreign('user1_deleted_message_id')->references('id')->on('messages');
            // $table->integer('user2_deleted_message_id')->unsigned()->index();
            // $table->foreign('user2_deleted_message_id')->references('id')->on('messages');
            // $table->text('latest_message_excerpt')->nullable();
            // $table->timestamp('latest_message_time')->nullable();
            // $table->boolean('latest_message_seen')->default(false);        
            // $table->integer('latest_message_sender_id')->unsigned()->index();
            // $table->foreign('latest_message_sender_id')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('is_accepted')->default(false);
        });
        // Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('conversations');
        Schema::enableForeignKeyConstraints();
    }
}
