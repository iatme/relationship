<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToEventsTableBaseOnDbV145 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints(); // disable foreign key checking strict to fix https://imwz.io/laravel-migration-cannot-add-update-child-row-foreign-key-constraint-fails/
        Schema::table('events', function (Blueprint $table) {

            $table->string('slug');
            $table->dateTime('start_time')->nullable();
            $table->dateTime('end_time')->nullable();
            $table->enum('status', ['draft','pending','published'])->nullable();
            $table->boolean('is_private')->default(false);
            $table->string('password',60)->nullable();
            $table->unsignedSmallInteger('attendee_count')->default(0);
            $table->unsignedSmallInteger('like_count')->default(0);
            $table->unsignedTinyInteger('venue_count')->default(0);
            $table->unsignedTinyInteger('staff_count')->default(0);
            $table->unsignedTinyInteger('organiser_count')->default(0);
            $table->string('featured_image')->nullable();

            $table->softDeletes();

            if (!Schema::hasColumn('events', 'created_at') && !Schema::hasColumn('events', 'updated_at')) {
                $table->timestamps();
            }

            
            $table->integer('author_id')->unsigned()->index();
            $table->foreign('author_id')->references('id')->on('users');
            
        });
        Schema::enableForeignKeyConstraints(); // re-enable foreign key checking strict
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {

            $table->dropColumn(['slug', 'start_time', 'end_time', 'status', 'is_private', 'password', 'attendee_count', 'like_count', 'venue_count', 'staff_count', 'organiser_count', 'featured_image', 'created_at', 'updated_at']);
            
            if (Schema::hasColumn('events', 'deleted_at')) {
                $table->dropColumn('deleted_at');
            }

            $table->dropForeign('events_author_id_foreign');
            $table->dropColumn('author_id');
        });
    }
}
