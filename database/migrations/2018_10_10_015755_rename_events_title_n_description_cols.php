<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameEventsTitleNDescriptionCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('events', function (Blueprint $table) {
            if (Schema::hasColumn('events', 'event_title')) {
                $table->renameColumn('event_title', 'title');
            }
            if (Schema::hasColumn('events', 'event_description')) {
                $table->renameColumn('event_description', 'description');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            if (Schema::hasColumn('events', 'description')) {
                $table->renameColumn('description', 'event_description');
            }
            if (Schema::hasColumn('events', 'title')) {
                $table->renameColumn('title', 'event_title');
            }
        });
    }
}
