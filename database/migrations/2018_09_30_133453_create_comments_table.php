<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('commentator_id')->unsigned();
            $table->text('comment');
            $table->integer('topic_id')->unsigned();
            $table->timestamps();

            $table->foreign('commentator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('topic_id')->references('id')->on('topics')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments', function (Blueprint $table) {
            if (Schema::hasColumn('comments', 'commentator_id'))
            {
                $table->dropForeign(['commentator_id']);
            }
            if (Schema::hasColumn('comments', 'topic_id'))
            {
                $table->dropForeign(['topic_id']);
            }
        });
        Schema::dropIfExists('comments');
    }
}
