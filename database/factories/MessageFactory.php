<?php

use Faker\Generator as Faker;

$factory->define(App\Message::class, function (Faker $faker) {
    return [
        'conversation_id'=> 3,
        'sender_id'      => 1,
        'body'			 => $faker->paragraph,
        'is_seen'		 => false,
    ];
});
