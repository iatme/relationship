<?php

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'commentator_id' => App\User::all(['id'])->random()->id,
        'comment' => $faker->paragraph,
        'topic_id' => App\Topic::all(['id'])->random()->id
    ];
});
