<?php

use Faker\Generator as Faker;

$factory->define(App\Topic::class, function (Faker $faker) {
    return [
        'event_id' => App\Event::all(['id'])->random()->id,
        'title' => $faker->sentence,
        'body' => $faker->paragraph,
        'is_pinned' => $faker->boolean
    ];
});
