export const AUTH_USER = "AUTH_USER";
export const LOGOUT_USER = "LOGOUT_USER";
export const AUTH_ERROR = "AUTH_ERROR";
export const AUTH_USER_INFO = "AUTH_USER_INFO";
export const USER_INFO = "USER_INFO";
export const USER_INFO_SUCCESS = "USER_INFO_SUCCESS";
