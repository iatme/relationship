import axios from 'axios';
import  history  from '../helpers/history';
import {
  AUTH_USER,
  AUTH_ERROR,
  LOGOUT_USER,
  USER_INFO_SUCCESS
} from './types'

// const ROOT_URL = 'http://huy.localhost'

export const userActions = {
    loginUser,
    logoutUser,
    userInfo
}


// User and Auth actions
//
// export const loginUser=({email,password}) =>{
function loginUser({email,password,remember_me}){
  return (dispatch)=>{
    return axios.post(url_api +'/api/auth/login',{email,password,remember_me})
    // return axios.post('/api/auth/login',{email,password,remember_me})
        .then(response => {
          dispatch({
            type: AUTH_USER,
            payload:response.data.access_token
          });
          localStorage.setItem('laravel_user_token',response.data.access_token);
          history.push('/home')
        }).catch(error => {
          dispatch(authError("User Infomation Unauthorized"));
          })
  }
}
export function authError(error){
  return {
    type:AUTH_ERROR,
    payload:error
  }
}


export const userInfo = () => {
  const token = localStorage.getItem('laravel_user_token')
  return (dispatch)=>{
    return axios.get(url_api+ '/api/auth/me',
          {
            headers: { 'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/x-www-form-urlencoded'
                      }
          }).then(response => {
            dispatch({
              type: USER_INFO_SUCCESS,
              payload: response.data
            })
          })
  }
}

// export const logoutUser = () => {
function logoutUser(){
  localStorage.removeItem('laravel_user_token')

  return { type: LOGOUT_USER }
}
// export function logoutUser() {
//   localStorage.removeItem('laravel_user_token');
//
//   return { type: LOGOUT_USER };
// }
