import {
  AUTH_USER,
  UNAUTH_USER,
  AUTH_ERROR,
  LOGOUT_USER,
  USER_INFO_SUCCESS
} from '../actions/types';
import redux , {createStore} from 'redux'
const DETAULT_STATE = {
  authenticated: (localStorage.getItem('laravel_user_token') !== null),
  userinfo: {
    name: null,
  }
};

function authReducer(state = DETAULT_STATE, action) {
  switch (action.type) {
    case USER_INFO_SUCCESS:
      return {
        ...state,
        userinfo: action.payload
      };
    case AUTH_USER:
      return {
        ...state,
        authenticated: true,
        // access_token: action.payload
      };
      case AUTH_ERROR:
      return {
        ...state,
        error: action.payload
      };
    case LOGOUT_USER:
      return {
        ...state,
        authenticated: false
      };

    default:
      // console.log(state);
      return state;
  }
}
export default authReducer
