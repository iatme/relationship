
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this applications
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// require('./components/AppBoot');
import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter,Router, Route } from 'react-router-dom'
import App from './components/App'
import {AUTH_USER,USER_INFO_SUCCESS} from './actions/types'
import { Provider } from 'react-redux'
import { store } from './helpers'
import history from './helpers/history'


//example for use store to dispatch
store.subscribe(() => {console.log(store.getState());});
//end example

const token = localStorage.getItem('laravel_user_token')
if(token){
  store.dispatch({type:AUTH_USER});
}

render((
  <Provider store={store}>
    <Router history={history}>
      <App />
    </Router>
</Provider>
), document.getElementById('root'));
