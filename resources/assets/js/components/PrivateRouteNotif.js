import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRouteNotif = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        signupstate ? <Component {...props} />
        : <Redirect to={{ pathname: '/account', state: { from: props.location } }} />
    )} />
)
