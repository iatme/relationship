import React , {Component} from 'react'
import { Link } from 'react-router-dom'
import {connect} from 'react-redux'
import {userActions} from '../../actions/useractions'
import * as actions from '../../actions/useractions'

// The Header creates links that can be used to navigate
// between routes.
// const HeaderAdmin = () => (
class HeaderAdmin extends Component {
  constructor(props){
    super(props)
    this.state = {
      auth:{
        authenticated:false
      }
    }
  }
  componentWillMount(){
    this.props.userInfo();
  }
  render(){
    return(
      <nav className="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
          <div className="container" >
            <div className='leftnav'>
              <Link className="navbar-brand js-scroll-trigger text-light" to="/">munifyevent</Link>
              <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
              </button>
            </div>
            <div className="navbar-collapse collapse" id="navbarResponsive">
              <div className='search-section'>
                <form className="form-inline">
                  <div className="input-group">
                    <div className="input-group-prepend">
                      <span className="input-group-text fa fa-search form-control-feedback " id="search-icon"></span>
                    </div>
                    <input className="form-control mr-sm-2" id='search-input' type='search' placeholder='Search' aria-label='Search'/>
                  </div>
                </form>
              </div>
              <div className='filter-section'>
                <form className="form-inline">
                  <div className="input-group">
                    <select data-filter="status" className="droplist-bg filter form-control">
                      <option value="">All</option>
                      <option value="">Comming</option>
                      <option value="">Pass</option>
                    </select>
                  </div>
                </form>
              </div>
              <div className='create-event ml-auto'>
                  <Link className="nav-link js-scroll-trigger text-light " id='create-btn' to='/home/create'>Create Event</Link>
              </div>
                <ul className="navbar-nav navbar-right" id='navright-admin'>
                  <li className="nav-item dropdown my-auto">
                      <a className="nav-link dropdown text-light " href="/message">
                          <i className="fas fa-envelope" style={{ fontSize : 20 }}></i>
                      </a>
                  </li>
                  <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle text-light" id="navbarDropdown" href="/" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i className="fas fa-user" style={{ fontSize : 20 }}></i>
                    </a>
                    <div className="dropdown-menu dropdown-menu-right droplist-bg" aria-labelledby="navbarDropdown">
                        <Link className="dropdown-item text-light" to="/home">My Events</Link>
                        <Link className="dropdown-item text-light" to="/home/myprofile">View Profile</Link>
                        <Link className="dropdown-item text-light" to="/account/logout"><i className="fa fa-btn fa-sign-out"></i>Logout</Link>
                    </div>
                  </li>
                  <li className="nav-item dropdown my-auto">
                      <a className="nav-link dropdown text-light" id="notifyDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                          <i className="fa fa-bell" style={{ fontSize : 20 }}></i>
                      </a>
                      <div className="dropdown-menu dropdown-menu-right" aria-labelledby="notifyDropdown">
                          <a className="dropdown-item" href="#">Notification 1</a>
                          <a className="dropdown-item" href="#">Notification 2</a>
                          <a className="dropdown-item" href="#">Notification 3</a>
                      </div>
                  </li>
                  <li className="nav-item dropdown my-auto">
                      <a className="nav-link dropdown text-light" href="/help">
                          <i className="fa fa-question-circle-o" style={{ fontSize : 20 }}></i>
                      </a>
                  </li>
                </ul>
            </div>
          </div>
      </nav>
    )
  }
}
function mapStateToProps(state){
  return {
    userinfo: state.auth.userinfo,
    authenticated: state.auth.authenticated
  };
}
export default connect(mapStateToProps, actions)(HeaderAdmin);
