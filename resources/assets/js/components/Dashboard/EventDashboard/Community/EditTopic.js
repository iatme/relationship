// resources/assets/js/components/Dashboard/Community/EditTopic.js

import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { EditorState, convertToRaw, ContentState, convertFromRaw,convertFromHTML } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import draftToHtml from 'draftjs-to-html'
import htmlToDraft from 'html-to-draftjs'
import {stateFromHTML} from 'draft-js-import-html'
import {stateToHTML} from 'draft-js-export-html'
import EditorImage from '../../../Tools/EditorImage'

    class EditTopic extends Component {
      constructor (props) {
        super(props)
          this.state = {
            title: '',
            description: '',
            id:'',
            editorState: EditorState.createEmpty()
          }

        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleEditEvent = this.handleEditEvent.bind(this)
        this.onEditorStateChange = this.onEditorStateChange.bind(this)
      }
      /*get data from json to input field*/
      componentDidMount(){
        const topicID = this.props.match.params.tid
        console.log(topicID);
        const { history } = this.props
        axios.get(url_api + `/api/events/${topicID}`).then(response =>{
            this.setState({
                title:response.data.event_title,
                description:response.data.event_description,
                id:response.data.id,
                editorState:EditorState.createWithContent(ContentState.createFromBlockArray(htmlToDraft(response.data.event_description).contentBlocks))
                // editorState:EditorState.createWithContent(stateFromHTML(response.data.event_description))
            })
        })
        .catch(error =>{
          console.log(error)
          history.push('/home/error')
        })
      }
      //function get state of editor when onchange calling
      onEditorStateChange(editorState){
        this.setState({editorState})
      }

      handleFieldChange (event) {
        this.setState({
          [event.target.name]: event.target.value
        })
      }
      //funciton calling when edit button submitted
      handleEditEvent (event) {
        event.preventDefault()
        const topicID = this.props.match.params.tid
        const eventID = this.props.match.params.eid
        const { history } = this.props
        const editevent = {
          event_title: this.state.title,
          // event_description: stateToHTML(this.state.editorState.getCurrentContent())
          event_description: draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))
        }
        axios.put(url_api + `/api/events/${topicID}`,editevent)
          .then(response => {
            // redirect to the topic detail
            history.push(`/home/dashboard/event/${eventID}/topic/${topicID}`)
          })
          .catch(error => {
            this.setState({
              status: error.response.data.status
            })
          })
      }
      render () {
        const {editorState} = this.state
        const eventID = this.props.match.params.eid
        const topicID = this.props.match.params.tid
        return (
          <section className='container'>
            <div className='row justify-content-center'>
              <div className='col-md-10'>
                <div className='card'>
                  <div className='card-header'>Edit my event</div>
                  <div className='card-body'>
                    <form onSubmit={this.handleEditEvent}>
                      <div className='form-group'>
                        <label htmlFor='title'>Event title</label>
                        <input
                          id='title'
                          type='text'
                          className='form-control'
                          name='title'
                          value={this.state.title}
                          onChange={this.handleFieldChange}
                          required
                        />
                      </div>
                      <div className='form-group'>
                        <label htmlFor='description'>Event description</label>
                        {/* <Editor
                          editorState={editorState}
                          wrapperClassName="demo-wrapper"
                          editorClassName="demo-editor"
                          onEditorStateChange={this.onEditorStateChange}
                        /> */}
                        <EditorImage
                          editorState={editorState}
                          wrapperClassName="demo-wrapper"
                          editorClassName="demo-editor"
                          onEditorStateChange={this.onEditorStateChange}
                        />
                        <textarea
                            disabled
                            value={draftToHtml(convertToRaw(editorState.getCurrentContent()))}
                          />
                        {/* <textarea
                          id='description'
                          className='form-control'
                          name='description'
                          rows='10'
                          value={this.state.description}
                          onChange={this.handleFieldChange}
                          required
                        /> */}
                      </div>
                      <div className='form-group'>
                        <button className='btn btn-primary'>Save</button>
                        <Link className='btn btn-primary' to={`/home/dashboard/event/${eventID}/topic/${topicID}`} >Cancel</Link>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </section>
        )
      }
    }

    export default EditTopic
