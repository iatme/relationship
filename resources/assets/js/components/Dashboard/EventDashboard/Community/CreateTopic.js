import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import EditorImage from '../../../Tools/EditorImage'
import { EditorState, convertToRaw,ContentState } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import draftToHtml from 'draftjs-to-html'
import {stateToHTML} from 'draft-js-export-html';
// import '../../../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

    class CreateTopic extends Component {
      constructor (props) {
        super(props)
        this.state = {
          title: '',
          description: '',
          errors: [],
          editorState: EditorState.createEmpty()
        }
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleCreateNewEvent = this.handleCreateNewEvent.bind(this)
        this.onEditorStateChange = this.onEditorStateChange.bind(this)
      }
      onEditorStateChange(editorState){
        this.setState({editorState})
      }
      handleFieldChange (event) {
        this.setState({
          [event.target.name]: event.target.value
        })
      }

      handleCreateNewEvent (event) {
        event.preventDefault()

        const { history } = this.props
        const eventID = this.props.match.params.eid
        const newevent = {
          event_title: this.state.title,
          // event_description: stateToHTML(this.state.editorState.getCurrentContent())
          event_description: draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))
        }
        console.log(newevent.event_description);
        axios.post(url_api + '/api/events',newevent)
          .then(response => {
            // redirect to the homepage
            history.push(`/home/dashboard/event/${eventID}/topic`)
          })
          .catch(error => {
            this.setState({
              errors: error.response.data.errors
            })
          })
      }
      render () {
        const {editorState} = this.state
        const eventID = this.props.match.params.eid
        return (
          <section className='container'>
            <div className='row justify-content-center'>
              <div className='col-md-10'>
                <div className='card'>
                  <div className='card-header'>Create New Topic</div>
                  <div className='card-body'>
                    <form onSubmit={this.handleCreateNewEvent}>
                      <div className='form-group'>
                        <label htmlFor='title'>Topic title</label>
                        <input
                          id='title'
                          type='text'
                          className='form-control'
                          name='title'
                          value={this.state.title}
                          onChange={this.handleFieldChange}
                          required
                        />
                      </div>
                      <div className='form-group'>
                        <label htmlFor='description'>Topic description</label>
                        {/* <Editor
                          editorState={this.state.editorState}
                          wrapperClassName="demo-wrapper"
                          editorClassName="demo-editor"
                          onEditorStateChange={this.onEditorStateHandleChange}
                        /> */}
                        <EditorImage
                          editorState={editorState}
                          wrapperClassName="demo-wrapper"
                          editorClassName="demo-editor"
                          onEditorStateChange={this.onEditorStateChange}
                        />
                      </div>
                      <div className='form-group'>
                        <button className='btn btn-primary'>Create</button>
                        <Link className='btn btn-primary' to={`/home/dashboard/event/${eventID}/topic`}>Cancel</Link>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </section>
        )
      }
    }

    export default CreateTopic
