// resources/assets/js/components/Dashboard/EventDashboard/Community/SingleTopic.js

import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class SingleTopic extends Component {
  constructor (props) {
    super(props)
    this.state = {
      // event: {},
      title: '',
      description: '',
      'id':''
    }
    this.handleDeleteEvent = this.handleDeleteEvent.bind(this)
  }

  componentDidMount () {
    const topicID = this.props.match.params.tid
    const { history } = this.props
    axios.get(url_api + `/api/events/${topicID}`).then(response => {
      this.setState({
        title:response.data.event_title,
        description:response.data.event_description,
        id:response.data.id,
      })
    })
    .catch(error=>{
      console.log(error)
      history.push('/home/error')
    })
  }

  /*delete a event*/
  handleDeleteEvent (event) {
    event.preventDefault()
    if (!confirm('Are your sure you want to delete this item?')) {
      return false
    }
    const { history } = this.props
    const topicID = this.props.match.params.tid
    const eventID = this.props.match.params.eid
    axios.delete(url_api + `/api/events/${topicID}`)
    .then(response => {
        // redirect to the homepage
        history.push(`/home/dashboard/event/${eventID}/topic`)
    })
    .catch(function (error) {
      console.log(error)
    })
  }


  render () {
    const eventID = this.props.match.params.eid
    const topicID = this.props.match.params.tid
    return (
      <section className='container'>
        {/* <NavEvent dataid={this.state.id}/> */}
        <div className='row justify-content-center'>
          <div className="page-header">
            <h2>Topic Dashboard</h2>
          </div>
          <div className='col-md-10'>
            <div className='card'>
              <div className='card-header'>{this.state.title}</div>
              <div className='card-body'>
                <p dangerouslySetInnerHTML={{__html: this.state.description}}></p>
                <Link className='btn btn-primary btn-sm mb-3' to={`/home/dashboard/event/${eventID}/topic/edit/${this.state.id}`}>
                  Edit
                </Link>
                <button className='btn btn-primary btn-sm mb-3' onClick={this.handleDeleteEvent}>
                  Delete
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default SingleTopic
