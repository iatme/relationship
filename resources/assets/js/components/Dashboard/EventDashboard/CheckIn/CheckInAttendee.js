// resources/assets/js/components/Dashboard/EventDashboard/CheckIn/CheckInAttendee.js

  import axios from 'axios'
  import React, { Component } from 'react'
  import { Link } from 'react-router-dom'

  class CheckInAttendee extends Component {
      constructor () {
        super()
        this.state = {
          events: []
        }
      }

      componentDidMount () {

        axios.get(url_api + '/api/events').then(response => {
          this.setState({
            events: response.data
          })
        })
        .catch(error => {
        	console.log(error)
        })
      }
      render () {
        const { events } = this.state
        return (
            <section className='container'>
              <div className='row justify-content-center'>
                <div className='col-md-12'>
                  <div className='card'>
                    <div className='card-header'>Check In Attendee</div>
                    <div className='card-body'>
                      <Link className='btn btn-primary btn-sm mb-3' to='/home/create'>
                        Create new event
                      </Link>
                      <ul className='list-group list-group-flush'>
                        {events.map(event => (
                          <Link
                            className='list-group-item list-group-item-action d-flex justify-content-between align-items-center'
                            to={`/home/dashboard/event/${event.id}`}
                            key={event.id} >
                            {event.event_title}
                            <span className='badge badge-primary badge-pill'>
                            </span>
                          </Link>
                        ))}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </section>
        )
      }
    }

    export default CheckInAttendee
