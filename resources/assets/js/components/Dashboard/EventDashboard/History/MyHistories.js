// resources/assets/js/components/Dashboard/AllEvent/EventsList.js

  import axios from 'axios'
  import React, { Component } from 'react'
  import { Link } from 'react-router-dom'

    class MyHistory extends Component {
      constructor () {
        super()
        this.state = {
          events: [],
          'background':'./frontend/img/eventlist-bg.jpg'
        }
      }

      componentDidMount () {

        axios.get(url_api + '/api/events').then(response => {
        // axios.get('/api/events').then(response => {
          this.setState({
            events: response.data.data,
          })
          console.log(response.data.data);
        })
        .catch(error => {
        	console.log(error)
        })
      }

      render () {
        const { events } = this.state
        // const Background = "./frontend/img/eventlist-bg.jpg"
        const Background = "../../../../frontend/img/eventlist-bg.jpg"
        // const sectionStyle = {
        //   // backgroundImage : `url("./frontend/img/eventlist-bg.jpg")`,
        //   backgroundImage: `url(${Background})`,
        //   backgroundSize : 'cover'
        // }
        return (
            <section className='container-fluid' id='list-esection'>
              <div className='d-md-inlineblock '>
                <ul>
                  {events.map(event => (
                    <Link className='events-list' key={event.id}
                      to='#'
                      // Background = {event.featured_image}
                      style={{
                              backgroundImage: `url(${event.featured_image?event.featured_image:Background})` ,
                              backgroundSize : 'cover'
                            }}
                      >
                      <div className='date-create'>
                        {/* 31 Oct {(new Date(event.updated_at)).toLocaleDateString()} */}
                        {new Intl.DateTimeFormat('en-GB', {
                                  // year: 'numeric',
                                  month: 'long',
                                  day: '2-digit'
                                }).format(new Date(event.updated_at))}
                      </div>
                      <div className="event_title">
                        <p>{event.title}</p>
                      </div>
                    </Link>
                  ))}
                </ul>
              </div>
            </section>
        )
      }
    }

    export default MyHistory
