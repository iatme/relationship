import React, {Component} from 'react'
import { Link } from 'react-router-dom'

const SidebarDashboard = ({match}) => (
       <div className="navbar-light h-100 sidebar" id='sidebarNav'>
         <ul className="navbar-nav sidebar-menu tree" data-widget="tree">
           <li className="nav-item ">
             <Link className="nav-link js-scroll-trigger text-light " to='#'>
               <i className="fas fa-signal mb-3 pr-3 sr-contact-2"></i><span>Dashboard</span>
             </Link>
           </li>
           <li className="nav-item ">
             <Link className="nav-link js-scroll-trigger text-light" to={`/home/dashboard/event/${match.params.eid}`}>
             <i className="fas fa-receipt mb-3 pr-3 sr-contact-2"></i><span>Event info</span>
            </Link>
           </li>
           <li className="nav-item ">
             <Link className="nav-link js-scroll-trigger text-light " to={`/home/dashboard/event/${match.params.eid}/contacts`}>
             <i className="fas fa-book-open mb-3 pr-3 sr-contact-2"></i><span>Attendee List</span>
            </Link>
           </li>
           <li className="nav-item ">
             <Link className="nav-link js-scroll-trigger text-light " to='#'>
             <i className="fas fa-comments mb-3 pr-3 sr-contact-2"></i><span>Community</span>
            </Link>
           </li>
           <li className="nav-item treeview">
             <Link className="nav-link text-light " to={`/home/dashboard/event/${match.params.eid}`}>
                <i className="fas fa-file-alt mb-3 pr-3 sr-contact-2"></i><span>Attachment</span>
                {/* <span className="pull-right-container">
                 <i className="fa fa-angle-left pull-right"></i>
               </span> */}
             </Link>
             <ul className='treeview-menu' style={{display: 'none'}}>
               <li className='nav-item'>
                 <Link className="nav-link text-light " to={`/home/dashboard/event/${match.params.eid}/images`}>
                 Images
                </Link>
               </li>
               <li className='nav-item'>
                 <Link className="nav-link text-light " to={`/home/dashboard/event/${match.params.eid}/documents`}>Documents</Link>
               </li>
             </ul>
           </li>
           <li className="nav-item treeview">
             <Link className="nav-link js-scroll-trigger text-light " to='#'>
             <i className="fas fa-user-check mb-3 pr-3 sr-contact-2"></i><span>Check in</span>
            </Link>
           </li>
           <li className="nav-item treeview">
             <Link className="nav-link js-scroll-trigger text-light " to='#'>
              <i className="fas fa-history mb-3 pr-3 sr-contact-2"></i><span>History</span>
            </Link>
           </li>
         </ul>
       </div>
)
export default SidebarDashboard
