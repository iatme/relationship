import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SingleEvent from '../AllEvent/SingleEvent'
import EditEvent from '../AllEvent/EditEvent'
import AllTopic from './Community/AllTopic'
import CreateTopic from './Community/CreateTopic'
import SingleTopic from './Community/SingleTopic'
import EditTopic from './Community/EditTopic'
import Contacts from './ContactBook/Contacts'
import CheckInAttendee from './CheckIn/CheckInAttendee'
import ImagesStore from './Storage/ImagesStore'
import DocumentsStore from './Storage/DocumentsStore'
import MyHistories from './History/MyHistories'
import SidebarDashboard from './SidebarDashboard'
import NoContentPage from '../../ErrorPage/NoContentPage'
import { Switch, Route } from 'react-router-dom'

const EventDashboard = () => (
// class EventDashboard extends Component
  <div className='row wrapper' id='edashboard'>
      <div className='col-md-2 main-sidebar' id='wrapper'>
        <Route path='/home/dashboard/event/:eid' component={SidebarDashboard}/>
      </div>
      <div className='col-md-10'>
        <Switch>
          <Route exact path='/home/dashboard/event/:eid' component={SingleEvent}/>
          <Route exact path='/home/dashboard/event/:eid/edit' component={EditEvent}/>
          <Route exact path='/home/dashboard/event/:eid/topic' component={AllTopic}/>
          <Route exact path='/home/dashboard/event/:eid/topic/create' component={CreateTopic}/>
          <Route exact path='/home/dashboard/event/:eid/topic/:tid' component={SingleTopic}/>
          <Route exact path='/home/dashboard/event/:eid/topic/edit/:tid' component={EditTopic}/>
          <Route exact path='/home/dashboard/event/:eid/contacts' component={Contacts}/>
          <Route exact path='/home/dashboard/event/:eid/checkin' component={CheckInAttendee}/>
          <Route exact path='/home/dashboard/event/:eid/images' component={ImagesStore}/>
          <Route exact path='/home/dashboard/event/:eid/documents' component={DocumentsStore}/>
          <Route exact path='/home/dashboard/event/:eid/checkin' component={CheckInAttendee}/>
          <Route exact path='/home/dashboard/event/:eid/histories' component={MyHistories}/>
          <Route path='/home/dashboard/event/:eid/*' component={NoContentPage}/>
        </Switch>
      </div>
  </div>
);

export default EventDashboard
