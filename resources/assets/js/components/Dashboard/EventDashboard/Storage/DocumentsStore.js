// resources/assets/js/components/Dashboard/EventDashboard/Storage/FileStorage.js

  import axios from 'axios'
  import React, { Component } from 'react'
  import { Link } from 'react-router-dom'

  class DocumentsStore extends Component {
      constructor () {
        super()
        this.state = {
          events:[],
          emptyfolder:0
        }
      }
      componentDidMount () {
        const eventID = this.props.match.params.eid
        console.log(eventID);
        axios.get(url_api + `/api/events/${eventID}/documents`).then(response => {
          this.setState({
            events: response.data.data
          })
          if(this.state.events.length){
            console.log( "rong");
          }
        })
        .catch(error => {
        	console.log(error)
        })
      }
      render () {
        const { events } = this.state
        if(events.length){
          return (
              <div className='container-fluid image-store' id='list-esection'>
                <div className='store-header row'>
                  <div className='event-title col-md-4'>
                    <h3>
                      <i class="fas fa-file-archive"></i><strong>Documents Store</strong>
                    </h3>
                  </div>
                  <div className='form-group new-event col-md-8'>
                    <button className='btn btn-primary'>Upload</button>
                  </div>
                </div>
                <div className='d-md-inlineblock image-detail'>
                  <ul>
                    {events.map(event => (
                      <Link className='events-list' key={event.model_id}
                        to={`/home/dashboard/event/${event.model_id}`}
                        // Background = {event.featured_image}
                        style={{
                                // backgroundImage: `url(${event.featured_image})` ,
                                backgroundSize : 'cover'
                              }}
                        >
                        <div className='date-create'>
                          {/* 31 Oct {(new Date(event.updated_at)).toLocaleDateString()} */}
                          {new Intl.DateTimeFormat('en-GB', {
                                    // year: 'numeric',
                                    month: 'long',
                                    day: '2-digit'
                                  }).format(new Date(event.updated_at))}
                        </div>
                        <div className="event_title">
                          <p>{event.file_name}</p>
                        </div>
                      </Link>
                    ))}
                  </ul>
                </div>
              </div>
          )
        }else{
          return (
              <div className='container-fluid image-store' id='list-esection'>
                <div className='store-header row'>
                  <div className='event-title col-md-4'>
                    <h3>
                      <i className="fas fa-file-archive"></i><strong>Documents Store</strong>
                    </h3>
                  </div>
                  <div className='form-group new-event col-md-8'>
                    <button className='btn btn-primary'>Upload</button>
                  </div>
                </div>
                <div className='d-md-inlineblock image-detail'>
                  <p className='message-empty'>Folder is empty</p>
                </div>
              </div>
          )
        }
      }
    }

    export default DocumentsStore
