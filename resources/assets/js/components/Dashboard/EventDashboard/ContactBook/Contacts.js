// resources/assets/js/components/Dashboard/EventDashboard/ContactBook/Contacts.js

  import axios from 'axios'
  import React, { Component } from 'react'
  import { Link } from 'react-router-dom'
  import users from './userdata.json'
  class Contacts extends Component {
      render () {
        return (
            <section className='container' id='contacts'>
              <div className='row justify-content-center'>
                <div className='col-lg-10 mx-auto text-center pb-4'>
                  <h1>
                    <strong>All Attendees</strong>
                  </h1>
                </div>
                <div className='col-md-12'>
                  <div className='card'>
                    <div className='card-body d-md-inlineblock '>
                      <ul className='list-group list-group-flush d-md-block'>
                        {users.map(user => (
                          <li className='list-group-item list-group-item-action justify-content-between align-items-center userdetail' key={user.id}>
                            <img src={user.avatar} />
                            <p>{user.name}</p>
                            <p>{user.email}</p>
                            <span className='badge badge-primary badge-pill'>
                            </span>
                          </li>
                          // {/* <Link */}
                            // className='list-group-item list-group-item-action d-flex justify-content-between align-items-center'
                            // to={`/home/dashboard/event/${user.id}`}
                            // key={user.id} >
                            // <img src={user.avatar}/>
                            // <p>{user.name}</p>
                            // <p>{user.email}</p>
                            // <span className='badge badge-primary badge-pill'>
                            // </span>
                          // </Link>
                        ))}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </section>
        )
      }
    }

    export default Contacts
