import React , {Component} from 'react'
import { Link } from 'react-router-dom'
import {connect} from 'react-redux'
import {userActions} from '../../../actions/useractions'
import * as actions from '../../../actions/useractions'


// The Header creates links that can be used to navigate
// between routes.
// const HeaderAdmin = () => (
class MyProflie extends Component {
  constructor(props){
    super(props)
    this.state = {
      auth:{
        authenticated:false
      }
    }
  }
  componentWillMount(){
    this.props.userInfo();
  }
  render(){
    return(
      <section className='container'>
        <div className='row justify-content-center'>
          <div className='col-md-10'>
            <div className='card'>
              <div className='card-header text-center'><h1>My Profile</h1></div>
              <div className='row card-body'>
                <div className='col-md-2 card'>
                  <img className="card-img-top" src={this.props.userinfo.avatar} alt="User image"/>
                </div>
                <div className='col-md-10'>
                  <div className='row'>
                    <div className='col-md-2 list-group-item'><label htmlFor='name'>Name</label></div>
                    <div className='col-md-10 list-group-item'>{this.props.userinfo.name}</div>
                  </div>
                  <div className='row'>
                    <div className='col-md-2 list-group-item'><label htmlFor='email'>Email</label></div>
                    <div className='col-md-10 list-group-item'>{this.props.userinfo.email}</div>
                  </div>
                  <div className='row'>
                    <div className='col-md-2 list-group-item'><label htmlFor='email'>Email</label></div>
                    <div className='col-md-10 list-group-item'>{this.props.userinfo.email}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}
function mapStateToProps(state){
  return {
    userinfo: state.auth.userinfo,
    authenticated: state.auth.authenticated
  };
}
export default connect(mapStateToProps, actions)(MyProflie);
