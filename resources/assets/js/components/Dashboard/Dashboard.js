import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import HeaderAdmin from './HeaderAdmin'
import EventsList from './AllEvent/EventsList'
import SingleEvent from './AllEvent/SingleEvent'
import EventDashboard from './EventDashboard/EventDashboard'
import EditEvent from './AllEvent/EditEvent'
import NewEvent from './AllEvent/NewEvent'
// import UploadMyFile from './AllEvent/UploadMyFile'
import MyProfile from './UserProfile/MyProfile'
import NoContentPage from '../ErrorPage/NoContentPage'
import { Switch, Route } from 'react-router-dom'


const Dashboard = () => (
  <div id='dashboard-admin'>
    <HeaderAdmin/>
      <Switch>
        <Route exact path='/home' component={EventsList}/>
        <Route path='/home/create' component={NewEvent}/>
        <Route path='/home/dashboard/event/:eid' component={EventDashboard}/>
        {/* <Route path='/dashboard/event/:id' component={SingleEvent}/>
        <Route path='/dashboard/edit/:id' component={EditEvent}/> */}
        <Route path='/home/myprofile' component={MyProfile}/>
        {/* <Route path='/home/upload' component={UploadMyFile}/> */}
        <Route path='/home/*' component={NoContentPage} />
      </Switch>
  </div>
)


export default Dashboard
