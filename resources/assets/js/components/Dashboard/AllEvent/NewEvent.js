import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import EditorImage from '../../Tools/EditorImage'
import { EditorState, convertToRaw,ContentState } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import draftToHtml from 'draftjs-to-html'
import {stateToHTML} from 'draft-js-export-html'
import {connect} from 'react-redux'
import {userActions} from '../../../actions/useractions'
import * as actions from '../../../actions/useractions'


    class NewEvent extends Component {
      constructor (props) {
        super(props)
        this.state = {
          title: '',
          description: '',
          errors: [],
          editorState: EditorState.createEmpty(),
          auth:{
            authenticated:false
          },
          selectedfile: null
        }
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleCreateNewEvent = this.handleCreateNewEvent.bind(this)
        this.onEditorStateChange = this.onEditorStateChange.bind(this)
        this.fileChangedHandler = this.fileChangedHandler.bind(this)
        // this.uploadHandler = this.uploadHandler.bind(this)
      }
      componentWillMount(){
        this.props.userInfo();
      }

      fileChangedHandler(event){
        const file = event.target.files[0]
        this.setState({selectedfile:event.target.files[0]})
      }

      onEditorStateChange(editorState){
        this.setState({editorState})
      }

      handleFieldChange (event) {
        this.setState({
          [event.target.name]: event.target.value
        })
      }

      handleCreateNewEvent (event) {
        event.preventDefault()
        const { history } = this.props
        const token = localStorage.getItem('laravel_user_token')
        var form = new FormData()
        form.append("images", this.state.selectedfile)
        form.append("title", this.state.title)
        form.append("description", draftToHtml(convertToRaw(this.state.editorState.getCurrentContent())))
        form.append("author_id", this.props.userinfo.id)

        var settings = {
          "url": url_api + '/api/events',
          "method": "POST",
          "headers": {
            "Content-Type": "multipart/form-data",
            "Authorization": "Bearer " + token,
          },
          "processData": false,
          "contentType": false,
          "mimeType": "multipart/form-data",
          "data": form
        }
        axios(settings)
          .then(function (response) {
            history.push('/home')
          }).catch(error=>{
              console.log(error);
            });
      }
      render () {
        const {editorState} = this.state
        return (
          <section className='container'>
            <div className='row justify-content-center'>
              <div className='col-lg-10 mx-auto text-center pb-4'>
                <h1>
                  <strong>Create new event</strong>
                </h1>
              </div>
              <div className='col-md-10'>
                <div className='card'>
                  <div className='card-body'>
                    <form onSubmit={this.handleCreateNewEvent} id='form-create'>
                      <div className='form-group'>
                        <label htmlFor='title'><i className="fas fa-file-signature mb-3 pr-3 sr-contact-2"></i>Event title</label>
                        <input
                          id='title'
                          type='text'
                          className='form-control'
                          name='title'
                          value={this.state.title}
                          onChange={this.handleFieldChange}
                        />
                      </div>
                      <div className='form-group'>
                        <label htmlFor='description'><i className="fas fa-file-signature mb-3 pr-3 sr-contact-2"></i>Event description</label>
                        <EditorImage
                          editorState={editorState}
                          wrapperClassName="demo-wrapper"
                          editorClassName="demo-editor"
                          onEditorStateChange={this.onEditorStateChange}
                        />
                      </div>
                      <div className='form-group'>
                        <label htmlFor='images'><i className="fas fa-images mb-3 pr-3 sr-contact-2"></i>Featured Image</label>
                        <input
                          id="upimages"
                          type="file"
                          className='form-control'
                          name='images'
                          onChange={this.fileChangedHandler}
                          selectedfile={this.selectedfile}/>
                      </div>
                      <div className='form-group new-event'>
                        <button className='btn btn-primary'>Create</button>
                        <Link className='btn btn-primary' to='/home'>Cancel</Link>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </section>
        )
      }
    }

    function mapStateToProps(state){
      return {
        userinfo: state.auth.userinfo,
        authenticated: state.auth.authenticated
      };
    }
    export default connect(mapStateToProps, actions)(NewEvent);
