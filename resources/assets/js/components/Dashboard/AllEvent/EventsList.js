// resources/assets/js/components/Dashboard/AllEvent/EventsList.js

  import axios from 'axios'
  import React, { Component } from 'react'
  import { Link } from 'react-router-dom'

    class EventsList extends Component {
      constructor () {
        super()
        this.state = {
          events: [],
          'background':'./frontend/img/eventlist-bg.jpg'
        }
      }

      componentDidMount () {

        axios.get(url_api + '/api/events').then(response => {
        // axios.get('/api/events').then(response => {
          this.setState({
            events: response.data.data,
          })
        })
        .catch(error => {
        	console.log(error)
        })
      }
      reloadPage () {
        window.location.reload()
      }
      render () {
        const { events } = this.state
        const Background = "./frontend/img/eventlist-bg.jpg"
        return (
            <section className='container-fluid' id='list-esection'>
              <div className='d-md-inlineblock '>
                <ul>
                  {events.map(event => (
                    <Link className='events-list' key={event.id}
                      to={`/home/dashboard/event/${event.id}`}
                      onClick={()=>this.reloadPage()}
                      // Background = {event.featured_image}
                      style={{
                              backgroundImage: `url(${event.featured_image?event.featured_image:Background})` ,
                              backgroundSize : 'cover'
                            }}
                      >
                      <div className='date-create'>
                        {new Intl.DateTimeFormat('en-GB', {
                                  // year: 'numeric',
                                  month: 'long',
                                  day: '2-digit'
                                }).format(new Date(event.updated_at))}
                      </div>
                      <div className="event_title">
                        <p>{event.title}</p>
                      </div>
                    </Link>
                  ))}
                </ul>
              </div>
            </section>
        )
      }
    }

    export default EventsList
