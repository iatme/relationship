// resources/assets/js/components/Dashboard/SingleEvent.js

import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class SingleEvent extends Component {
  constructor (props) {
    super(props)
    this.state = {
      // event: {},
      title: '',
      description: '',
      id:'',
      featured_image:null,
      updated_at:'',
      // location:'',
      attendees:'',
      organizer:'',

    }
    this.handleDeleteEvent = this.handleDeleteEvent.bind(this)
  }

  componentDidMount () {
    const eventID = this.props.match.params.eid
    const { history } = this.props
    axios.get(url_api + `/api/events/${eventID}`).then(response => {
      this.setState({
        title: response.data.data.title,
        description: response.data.data.description,
        id: response.data.data.id,
        featured_image: response.data.data.featured_image,
        updated_at: response.data.data.updated_at,
        // location: response.data.data.location,
        attendees: response.data.data.attendee_count,
        organizer: response.data.data.organiser_count

      })
    })
    .catch(error=>{
      console.log(error)
      history.push('/home/error')
    })
  }

  /*delete a event*/
  handleDeleteEvent (event) {
    event.preventDefault()
    if (!confirm('Are your sure you want to delete this item?')) {
      return false
    }
    const { history } = this.props
    const eventID = this.props.match.params.eid
    axios.delete(url_api + `/api/events/${eventID}`)
    .then(response => {
        // redirect to the homepage
        history.push('/home')
    })
    .catch(function (error) {
      console.log(error)
    })
  }


  render () {
    const Background = url_api + "/frontend/img/eventlist-bg.jpg"
    return (
      <div className='container' id='single-event'>
        <div className='row justify-content-center'>
          <div className='col-lg-12 mx-auto text-center pb-4'>
            <div className='container'>
              <div className='feature-image'
                style={{
                      backgroundImage: `url(${this.state.featured_image?this.state.featured_image:Background})`
                      // backgroundSize : 'cover'
                    }}>
              </div>
            </div>
          </div>
          <div className='col-md-12'>
            <div className='event-title'>
              <h3>
                <strong>{this.state.title}</strong>
              </h3>
            </div>
            <div className='row'>
              <div className='col-md-4 event-detail'>
                <ul className=''>
                  <li className=''>
                    <p className='event-item'><i className="fas fa-calendar-alt mb-3 pr-3 sr-contact-2"></i>Date: {(new Date(this.state.updated_at)).toLocaleDateString()}
                    </p>
                  </li>
                  <li className=''>
                    <p className='event-item'><i className="fas fa-map-marker-alt mb-3 pr-3 sr-contact-2"></i>Location</p>
                  </li>
                  <li className=''>
                    <p className='event-item'><i className="fas fa-user mb-3 pr-3 sr-contact-2"></i>Organizer: {this.state.organizer}</p>
                  </li>
                  <li className=''>
                    <p className='event-item'><i className="fas fa-users mb-3 pr-3 sr-contact-2"></i>Attendees: {this.state.attendees}</p>
                  </li>
                </ul>
              </div>
              <div className='col-md-8 event-description'>
                <p className='event-item'><i className="fas fa-edit mb-3 pr-3 sr-contact-2"></i>Description</p>
                <p dangerouslySetInnerHTML={{__html: this.state.description}}></p>
              </div>
            </div>
            <div className='event-sponsors col-md-5'>
              <p className='event-item'><i className="fas fa-user mb-3 pr-3 sr-contact-2"></i>Sponsors</p>
            </div>
            <div className='images-section d-md-inlineblock'>
              <ul>
                <li className='image-item'>Sponsors 1</li>
                <li className='image-item'>Sponsors 2</li>
                <li className='image-item'>Sponsors 3</li>
                <li className='image-item'>Sponsors 4</li>
                <li className='image-item'>Sponsors 5</li>
                <div className='image-item'>
                  <a href='#'>ADD</a>
                </div>
              </ul>
            </div>
            <div className='event-doc col-md-5'>
              <p className='event-item'><i className="fas fa-book mb-3 pr-3 sr-contact-2"></i>Announcement/Document</p>
            </div>
            <div className='images-section d-md-inlineblock'>
              <ul>
                <li className='image-item'>Document 1</li>
                <li className='image-item'>Document 2</li>
                <li className='image-item'>Document 3</li>
                <li className='image-item'>Document 4</li>
                <li className='image-item'>Document 5</li>
                <div className='image-item'>
                  <a href='#'>ADD</a>
                </div>
              </ul>
            </div>
            <div className='form-group new-event mb-0'>
              <Link className='btn btn-primary btn-sm mb-3' to={`/home/dashboard/event/${this.state.id}/edit`}>
                Edit
              </Link>
              <button className='btn btn-primary btn-sm mb-3' onClick={this.handleDeleteEvent}>
                Delete
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default SingleEvent
