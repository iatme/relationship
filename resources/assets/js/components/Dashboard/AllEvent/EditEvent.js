// resources/assets/js/components/Dashboard/AllEvent/EditEvent.js

import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { EditorState, convertToRaw, ContentState, convertFromRaw,convertFromHTML } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import draftToHtml from 'draftjs-to-html'
import htmlToDraft from 'html-to-draftjs'
import {stateFromHTML} from 'draft-js-import-html'
import {stateToHTML} from 'draft-js-export-html'
import EditorImage from '../../Tools/EditorImage'

    class EditEvent extends Component {
      constructor (props) {
        super(props)
          this.state = {
            title: '',
            description: '',
            id:'',
            editorState: EditorState.createEmpty(),
            selectedfile: null,
            featured_image:null
          }

        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleEditEvent = this.handleEditEvent.bind(this)
        this.onEditorStateChange = this.onEditorStateChange.bind(this)
        this.fileChangedHandler = this.fileChangedHandler.bind(this)

      }
      /*get data from json to input field*/
      componentDidMount(){
        const eventID = this.props.match.params.eid
        const { history } = this.props
        axios.get(url_api + `/api/events/${eventID}`).then(response =>{
            this.setState({
                title:response.data.data.title,
                description:response.data.data.description,
                id:response.data.data.id,
                editorState:EditorState.createWithContent(ContentState.createFromBlockArray(htmlToDraft(response.data.data.description).contentBlocks)),
                // editorState:EditorState.createWithContent(stateFromHTML(response.data.event_description))
                featured_image:response.data.data.featured_image
            })
        })
        .catch(error =>{
          console.log(error)
          history.push('/home/error')
        })
      }
      fileChangedHandler(event){
        const file = event.target.files[0]
        this.setState({selectedfile:event.target.files[0]})
      }
      //function get state of editor when onchange calling
      onEditorStateChange(editorState){
        this.setState({editorState})
      }

      handleFieldChange (event) {
        this.setState({
          [event.target.name]: event.target.value
        })
      }
      //funciton calling when edit button submitted
      handleEditEvent (event) {
        event.preventDefault()
        const eventID = this.props.match.params.eid
        const { history } = this.props
        const token = localStorage.getItem('laravel_user_token')
        var form = new FormData()
        if(this.state.selectedfile !== null ){
          form.append("images", this.state.selectedfile)
        }
        form.append("title", this.state.title)
        form.append("_method", "PUT")
        form.append("description", draftToHtml(convertToRaw(this.state.editorState.getCurrentContent())))

        var settings = {
          "url": url_api + `/api/events/${eventID}`,
          "method": "POST",
          "headers": {
            "Content-Type": "multipart/form-data",
            "Authorization": "Bearer " + token,
          },
          "processData": false,
          "contentType": false,
          "mimeType": "multipart/form-data",
          "data": form
        }
        axios(settings)
          .then(function (response) {
            history.push(`/home/dashboard/event/${eventID}`)
          }).catch(error=>{
              console.log(error);
            });
      }
      getFeaturedImage(){
        if(this.state.featured_image){
          return(
            <div className='form-control featured-image col-md-6'
              style={{
                    backgroundImage: `url(${this.state.featured_image})`,
                    backgroundSize : 'cover'
                  }}>
            </div>
          )
        }else{
          return(
            <p className='message-noimage'>No featured image</p>
          )
        }
      }
      render () {
        const {editorState} = this.state
        const event_id = this.props.match.params.eid
        const Background = url_api + "/frontend/img/eventlist-bg.jpg"

        return (
          <section className='container pt-5'>
            <div className='row justify-content-center'>
              <div className='col-lg-10 mx-auto text-center pb-4'>
                <h1>
                  <strong>Edit my event</strong>
                </h1>
              </div>
              <div className='col-md-10'>
                <div className='card'>
                  <div className='card-body'>
                    <form onSubmit={this.handleEditEvent} id='form-edit'>
                      <div className='form-group'>
                        <label htmlFor='title'><i className="fas fa-file-signature mb-3 pr-3 sr-contact-2"></i>Event title</label>
                        <input
                          id='title'
                          type='text'
                          className='form-control'
                          name='title'
                          value={this.state.title}
                          onChange={this.handleFieldChange}
                          required
                        />
                      </div>
                      <div className='form-group'>
                        <label htmlFor='description'><i className="fas fa-file-signature mb-3 pr-3 sr-contact-2"></i>Event description</label>
                        <EditorImage
                          editorState={editorState}
                          wrapperClassName="demo-wrapper"
                          editorClassName="demo-editor"
                          onEditorStateChange={this.onEditorStateChange}
                        />
                      </div>
                      <div className='form-group'>
                        <label htmlFor='images'><i className="fas fa-images mb-3 pr-3 sr-contact-2"></i>Featured Image</label>
                        {this.getFeaturedImage()}
                        <input
                          id="upimages"
                          type="file"
                          className='form-control'
                          name='images'
                          onChange={this.fileChangedHandler}
                          selectedfile={this.selectedfile}/>
                      </div>
                      <div className='form-group new-event'>
                        <button className='btn btn-primary'>Save</button>
                        <Link className='btn btn-primary' to={`/home/dashboard/event/${event_id}`} >Cancel</Link>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </section>
        )
      }
    }

    export default EditEvent
