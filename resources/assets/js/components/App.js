import React from 'react'
import Header from './LandingPage/Header'
import Main from './LandingPage/Main'

const App = () => (
  <div>
    <Main />
  </div>
)

export default App
