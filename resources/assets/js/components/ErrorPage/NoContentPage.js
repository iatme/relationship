import React from 'react'
import { Link } from 'react-router-dom'
import HeaderAdmin from '../Dashboard/HeaderAdmin'

  const NoContentPage = () => (
    <div>
      <section className='container'>
        <div className='row justify-content-center'>
          <div className='col-md-10'>
            <div className='card'>
              <div className='card-header text-center'><h1>Event is not exists</h1></div>
              <div className='card-body'>
                <p>Sorry, there is nothing to see here.</p>
                <p><Link to="/dashboard">Back to Dashboard</Link></p>
              </div>
            </div>
          </div>
        </div>
      </section>
  	</div>
  	)
  	export default NoContentPage
