  import React from 'react'
  import { Link } from 'react-router-dom'
  import Header from '../LandingPage/Header'

  const NotFoundPage = () => (
  	<div>
      <Header/>
      <section className='container'>
        <div className='row justify-content-center'>
          <div className='col-md-10'>
            <div className='card'>
              <div className='card-header text-center'><h1>Page Not Found</h1></div>
              <div className='card-body'>
                <p>Sorry, there is nothing to see here.</p>
                <p><Link to="/">Back to Home</Link></p>
              </div>
            </div>
          </div>
        </div>
      </section>
  	</div>
  	)
  	export default NotFoundPage
