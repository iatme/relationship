import React,{Component} from 'react';
import {reduxForm} from 'redux-form';
import * as actions from '../../actions/useractions';
import history from '../../helpers/history';
import { connect } from 'react-redux';
import {userActions} from '../../actions/useractions'
import {Redirect} from 'react-router-dom'

class Logout extends Component{

  componentWillMount(){
    this.props.dispatch(userActions.logoutUser());
  }
  render(){
    return(
      <Redirect to='/' />
    )
  }
}

export default connect()(Logout);
