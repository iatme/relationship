import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import HeaderLog from './HeaderLog'
import Login from './Login'
import LogOut from './LogOut'
import Register from './Register'
import CheckEmail from './CheckEmail'
import NotFoundPage from '../ErrorPage/NotFoundPage'
import { Switch, Route } from 'react-router-dom'
import { PrivateRouteNotif } from '../../components/PrivateRouteNotif'
import Footer from '../LandingPage/Footer'




const AuthAcc = () => (
  <div>
    <HeaderLog/>
    <Switch>
      <Route exact path='/account' component={Login}/>
      <Route path='/account/signup' component={Register}/>
      <PrivateRouteNotif path='/account/notification' component={CheckEmail}/>
      <Route path='/account/logout' component={LogOut}/>
      <Route path='/account/*' component={NotFoundPage} />
    </Switch>
    <Footer/>
  </div>
)


export default AuthAcc
