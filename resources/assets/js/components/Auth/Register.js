import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import HeaderLog from './HeaderLog'
import CheckEmail from './CheckEmail'


    class Register extends Component {
      constructor (props) {
        super(props)
        this.state = {
          name: '',
          email: '',
          password: '',
          password_confirmation:'',
        }
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleRegisterUser = this.handleRegisterUser.bind(this)
      }

      handleFieldChange (event) {
        this.setState({
          [event.target.name]: event.target.value
        })
      }

      handleRegisterUser (event) {
        event.preventDefault()

        const { history } = this.props
        const newuser = {
          name: this.state.name,
          email: this.state.email,
          password: this.state.password,
          password_confirmation: this.state.password_confirmation,
          message:''
        }

        axios.post(url_api + '/api/auth/signup',newuser)
          .then(response => {
            // redirect to the homepage
            signupstate = 1
             history.push('/account/notification')
          })
          .catch(error => {
            this.setState({
               message: error.response.data.message
            })
            console.log(error.response.data.message)
            var message = this.state.message
            if (!confirm('Signup fail ' + this.state.message)) {
              return false
            }
             // console.log(error);
          })
      }

      render () {
        return (
          <section className='container mx-auto' id='account-page'>
            <div className='row justify-content-center'>
              <div className='col-lg-10 mx-auto text-center pb-4'>
                <h1>
                  <strong>Sign up</strong>
                </h1>
              </div>
              <div className='col-md-5'>
                <div className='wrapper-content'>
                  <div className=''>
                    <form onSubmit={this.handleRegisterUser}>
                      <div className='form-group account'>
                        <input
                          id='name'
                          type='text'
                          className='form-control'
                          name='name'
                          placeholder='&#xf007; Name'
                          value={this.state.name}
                          onChange={this.handleFieldChange}
                          required
                        />
                      </div>
                      <div className='form-group account'>
                        <input
                          id='email'
                          type='email'
                          className='form-control'
                          name='email'
                          placeholder='&#xf0e0; Personal Email'
                          value={this.state.email}
                          onChange={this.handleFieldChange}
                          required
                        />
                      </div>
                      <div className='form-group account'>
                        <input
                          id='password'
                          type='password'
                          className='form-control'
                          name='password'
                          placeholder='&#xf084; Password'
                          value={this.state.password}
                          onChange={this.handleFieldChange}
                          required
                        />
                      </div>
                      <div className='form-group account'>
                        <input
                          id='password_confirmation'
                          type='password'
                          className='form-control'
                          name='password_confirmation'
                          placeholder='&#xf084; Confirmation Password'
                          value={this.state.password_confirmation}
                          onChange={this.handleFieldChange}
                          required
                        />
                      </div>
                      <div className='form-group text-center pt-3'>
                        <button className='btn btn-primary'>Sign up</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </section>
        )
      }
    }

    export default Register
