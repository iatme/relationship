import React,{Component} from 'react';
import {reduxForm} from 'redux-form';
import * as actions from '../../actions/useractions';
import history from '../../helpers/history';
import { connect } from 'react-redux';
import {userActions} from '../../actions/useractions'


class Login extends Component{
  constructor (props) {
    super(props)
    this.state = {
      'email':'',
      'password':'',
      'remember':0
    }
    if(!this.props.authenticated){

    }
    this.handleFieldChange = this.handleFieldChange.bind(this)
    this.handleUserLogin = this.handleUserLogin.bind(this)
    this.handleFieldChangeCHK = this.handleFieldChangeCHK.bind(this)
  }
  handleFieldChangeCHK(){
    this.setState({
      remember:!this.state.remember
    })
  }
  handleFieldChange (event) {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  handleUserLogin (event) {
    event.preventDefault()
    let {email, password,remember} = this.state
    const {dispatch} = this.props
    dispatch(userActions.loginUser({email,password,remember}))
  }
  renderAlert(){
      if(this.props.errorMessage){
        return (
          <div className="alert alert-danger">
           {this.props.errorMessage }
          </div>
        );
      }
    }
  render () {
  // // let login = localStorage.getItem('token')
  // // if(login){
  // //   this.props.history.push("/dashboard")
  // }
  let {email, password,remember} = this.state;
    return (
      <section className='container mx-auto' id='account-page'>
        <div className='row justify-content-center'>
          <div className='col-lg-10 mx-auto text-center pb-4'>
            <h1>
              <strong>Sign in</strong>
            </h1>
          </div>
          <div className='col-md-5'>
            <div className='wrapper-content'>
              <div className=''>
                <form onSubmit={this.handleUserLogin} name='LoginForm'>
                  <div className='form-group account'>
                    <input
                      id='email'
                      type='email'
                      className='form-control'
                      name='email'
                      placeholder='&#xf0e0; Email'
                      value={this.state.email}
                      onChange={this.handleFieldChange}
                      required
                    />
                  </div>
                  <div className='form-group account'>
                    <input
                      id='password'
                      type='password'
                      className='form-control'
                      name='password'
                      placeholder='&#xf084; Password'
                      value={this.state.password}
                      onChange={this.handleFieldChange}
                      required
                    />
                  </div>
                  <div className='form-group'>
                    <label htmlFor='remember'>
                    <input
                      id='remember'
                      type='checkbox'
                      name='remember'
                      // value='1'
                      // defaultChecked='false'
                      checked={this.state.remember}
                      onChange={this.handleFieldChangeCHK}
                    />
                    <span>Remember me</span>
                    </label>
                  </div>
                  {this.renderAlert()}
                  <div className='form-group text-center'>
                    <button className='btn btn-primary'>Sign in</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    authenticated:state.auth.authenticated
   }
}

// const mapDispatchToProps = (dispatch) => {
//   return{
//     loginUser: (email,password) =>dispatch(loginUser(email,password))
//   }
// }
// export default Login
export default connect(mapStateToProps)(Login)
