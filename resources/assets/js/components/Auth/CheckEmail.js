import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Register from './Register'



const CheckEmail = () => (
        <section className='container mx-auto' id='account-page'>
          <div className='row justify-content-center'>
            <div className='col-lg-10 mx-auto text-center pb-4'>
              <h1>
                <strong>Notification</strong>
              </h1>
            </div>
            <div className='col-md-5'>
              <div className='wrapper-content'>
                <div className=''>
                    <div className='form-group'>
                      <label
                      />You've been signed up. Please check your E-mail address to confirm your new account.
                    </div>
                </div>
              </div>
            </div>
          </div>
        </section>
  )

export default CheckEmail
