import React from 'react'
import { Link } from 'react-router-dom'

// The Header creates links that can be used to navigate
// between routes.
const HeaderLog = () => (
  <nav className="navbar navbar-expand-lg navbar-light navbar-shrink fixed-top" id="mainNav">
      <div className="container" >
          <Link className="navbar-brand js-scroll-trigger text-light" to="/">munifyevent</Link>
          <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="navbar-collapse collapse" id="navbarResponsive">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                  <a className="nav-link js-scroll-trigger text-light " href="#about">About</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link js-scroll-trigger text-light " href="#explore">Explore</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link js-scroll-trigger text-light " href="#contact">Contact</a>
                </li>
                <li className="nav-item ">
                  <Link className="nav-link js-scroll-trigger text-light " to='/account'>Sign in</Link>
                </li>
                <li className="nav-item ">
                  <Link className="nav-link js-scroll-trigger text-light " id='signup-btn' to='/account/signup'>sign up</Link>
                </li>
              </ul>
          </div>
      </div>
  </nav>
)

export default HeaderLog
