import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        localStorage.getItem('laravel_user_token')
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/account', state: { from: props.location } }} />
    )} />
)
