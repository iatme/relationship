import React from 'react'
import Header from './Header'
import Footer from './Footer'


const Home = () => (
  <div>
  <Header/>
    <header className="masthead text-center d-flex">
      <div className="container my-auto">
        <div className="row">
          <div className="col-lg-10 mx-auto">
            <h1 className="text-uppercase text-light">
              <strong>MunifyEvent</strong>
            </h1>
            <hr/>
          </div>
          <div className="col-lg-8 mx-auto text-light">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>
        </div>
      </div>
    </header>
     <section className="bg-primary" id="about">
      <div className="container">
        <div className="row">
          <div className="col-lg-8 mx-auto text-center">
            <h2 className="section-heading ">Hero header</h2>
            <hr className="light my-4"/>
            <p className="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            {/* <a className="btn btn-light btn-xl js-scroll-trigger" href="#services">Get Started!</a> */}
          </div>
        </div>
      </div>
    </section>
    <section id="features">
      <div className="container">
        <div className="row">
          {/* <div className="col-lg-12 text-center">
            <h2 className="section-heading">Features</h2>
          </div> */}
        </div>
      </div>
      <div className="container">
        <ul className="row pl-0 ">
          <li className="text-center list-features">
            <div className="service-box mx-auto">
              <span className='service-box-icon'>
              </span>
              <p className='title'>Feature 1</p>
              <p className='description'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
          </li>
          <li className="text-center list-features">
            <div className="service-box mx-auto">
              <span className='service-box-icon'>
              </span>
              <p className='title'>Feature 2</p>
              <p className='description'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
          </li>
          <li className="text-center list-features">
            <div className="service-box mx-auto">
              <span className='service-box-icon'>
              </span>
              <p className='title'>Feature 3</p>
              <p className='description'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
          </li>
          <li className="text-center list-features">
            <div className="service-box mx-auto">
              <span className='service-box-icon'>
              </span>
              <p className='title'>Feature 4</p>
              <p className='description'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
          </li>
          <li className="text-center list-features">
            <div className="service-box mx-auto">
              <span className='service-box-icon'>
              </span>
              <p className='title'>Feature 5</p>
              <p className='description'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
          </li>
        </ul>
      </div>
    </section>
    <section id="contact">
      <div className="container">
        <div className="row">
          <div className="col-md-4 text-left">
            <p className='title'>Lorem</p>
            <p className='description'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua.</p>
            <p className='name-contact'><i className="fas fa-user fa-2x mb-3 pr-3 sr-contact-2"></i>Name Surname</p>
          </div>
          <div className="col-md-4 text-left">
            <p className='title'>Lorem</p>
            <p className='description'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua.</p>
            <p className='name-contact'><i className="fas fa-user fa-2x mb-3 pr-3 sr-contact-2"></i>Name Surname</p>
          </div>
          <div className="col-md-4 text-left">
            <p className='title'>Lorem</p>
            <p className='description'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua.</p>
            <p className='name-contact'><i className="fas fa-user fa-2x mb-3 pr-3 sr-contact-2"></i>Name Surname</p>
          </div>
        </div>
      </div>
    </section>
    <Footer/>
  </div>
)

export default Home
