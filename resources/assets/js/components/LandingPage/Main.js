import React, {Component} from 'react'
import { Switch, Route } from 'react-router-dom'
import HomePage from '../LandingPage/HomePage'
import Login from '../Auth/Login'
import Register from '../Auth/Register'
import AuthAcc from '../Auth/AuthAcc'
import Example from '../Example'
import Dashboard from '../Dashboard/Dashboard'
// import FileUploadComponent from './Tools/FileUploadComponent'
import { PrivateRoute } from '../../components/PrivateRoute'
import NotFoundPage from '../ErrorPage/NotFoundPage'



// The Main component renders one of the three provided
// Routes (provided that one matches).The / route will only match
// when the pathname is exactly the string "/"
class Main extends Component{
  render(){
    return(
      <main>
        <Switch>
          <Route exact path='/' component={HomePage}/>
          <Route path='/account' component={AuthAcc}/>
          <PrivateRoute path='/home' component={Dashboard}/>
          <Route path='*' component={NotFoundPage} />
        </Switch>
      </main>
    )
  }
}

export default Main
