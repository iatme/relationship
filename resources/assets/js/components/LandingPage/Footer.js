import React , {Component} from 'react'
import { Link } from 'react-router-dom'

const Footer = () =>(
  <footer className="py-4">
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-6">
          <div className="container" id="left-footer">
            <div className="row">
              <div className="col-md-4">
                <p className='title'>Contact Us</p>
                <p>+44 34567890</p>
                <p>info@munifyevent.com</p>
                <p><a href="#">Download</a></p>
              </div>
              <div className="col-md-4">
                <p className='title'>Customer Services</p>
                <p><a href="#">Contact Us</a></p>
                <p><a href="#">Order and Payment</a></p>
                <p><a href="#">Shipping</a></p>
                <p><a href="#">Returns</a></p>
                <p><a href="#">FAQ</a></p>
              </div>
              <div className="col-md-4">
                <p className='title'>Information</p>
                <p><a href="#">About Munify event</a></p>
                <p><a href="#">Working With Us</a></p>
                <p><a href="#">Privacy and Policy</a></p>
                <p><a href="#">Terms & Conditions</a></p>
                <p><a href="#">Press Enquiries</a></p>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <div className="row">
            <div className="col-md-6">
              <p className='title'>Subscribe to Munify event via Email</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
              <form className="form-subscribe" action="" method="">
                <div className="form-group">
                  <input className="form-control" type="email" name="email"  placeholder="Email Address"/>
                </div>
                <div className="form-group mb-0">
                  <button className='btn btn-primary form-control'>Subscribe</button>
                </div>
              </form>
            </div>
            <div className="col-md-6">
              <p className="m-0">&copy; 2018 Munify event - design by Papagroup </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
)
export default Footer
