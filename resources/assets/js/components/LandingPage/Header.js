import React , {Component} from 'react'
import { Link } from 'react-router-dom'
import {connect} from 'react-redux'
import {userActions} from '../../actions/useractions'
import * as actions from '../../actions/useractions'
// The Header creates links that can be used to navigate
// between routes.
// const Header = () => (
class Header extends Component{
  constructor(props){
    super(props)
    this.state = {
      auth:{
        authenticated:false
      }
    }
  }
  renderLoginMenu(){
    if(this.props.authenticated){
      return(
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <a className="nav-link js-scroll-trigger text-light " href="#about">About</a>
          </li>
          <li className="nav-item">
            <a className="nav-link js-scroll-trigger text-light " href="#features">Features</a>
          </li>
          <li className="nav-item">
            <a className="nav-link js-scroll-trigger text-light " href="#contact">Contact</a>
          </li>
          <li className="nav-item ">
            <Link className="nav-link js-scroll-trigger text-light " to='/home'>My Events</Link>
          </li>
        </ul>
      )
    }
    else{
      return(
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <a className="nav-link js-scroll-trigger text-light " href="#about">About</a>
          </li>
          <li className="nav-item">
            <a className="nav-link js-scroll-trigger text-light " href="#features">Features</a>
          </li>
          <li className="nav-item">
            <a className="nav-link js-scroll-trigger text-light " href="#contact">Contact</a>
          </li>
          <li className="nav-item ">
            <Link className="nav-link js-scroll-trigger text-light " to='/account'>Login</Link>
          </li>
          <li className="nav-item ">
            <Link className="nav-link js-scroll-trigger text-light " id='signup-btn' to='/account/signup'>sign up</Link>
          </li>
        </ul>
      )
    }
  }
  render(){
    return(
      <nav className="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
          <div className="container" >
              <a className="navbar-brand js-scroll-trigger text-light" href="#page-top">munifyevent</a>
              <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="navbar-collapse collapse" id="navbarResponsive">
                    {this.renderLoginMenu()}
              </div>
          </div>
      </nav>

    )
  }
}

function mapStateToProps(state){
 return {
   userinfo: state.auth.userinfo,
   authenticated: state.auth.authenticated
 };
}

export default connect(mapStateToProps, actions)(Header);
