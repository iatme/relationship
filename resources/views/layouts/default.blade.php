<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    @yield('head')
    <script>
      var url_api = "{{ env("APP_URL") }}"
    </script>
    <title>MunifyEvent - @yield('title')</title>
  </head>
  <body id="page-top">
    @stack('style')
    @yield('navbar')
    <div class="main-content">
      @yield('content')
    </div>
    @stack('script')
  </body>
</html>
