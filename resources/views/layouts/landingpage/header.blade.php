<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">
<!--favicon  icon-->
<link rel="icon" type="image/png" href="{!! asset('frontend/images/icons/favicon.ico')!!}"/>
<!-- Bootstrap core CSS -->
<!-- <link href="{!! asset('frontend/css/bootstrap.min.css')!!}" rel="stylesheet"> -->

<!-- Custom fonts for this template -->
<link href="{!! asset('frontend/css/all.min.css')!!}" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{!! asset('frontend/css/font-awesome.css')!!}">

<!-- Plugin CSS -->
<link href="{!! asset('frontend/css/magnific-popup.css')!!}" rel="stylesheet">
<link href="{!! asset('css/app.css') !!}" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="{!! asset('frontend/css/creative.min.css')!!}" rel="stylesheet">
<link href="{!! asset('frontend/css/style.css')!!}" rel="stylesheet">
