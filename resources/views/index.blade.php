@extends('layouts.default')
@section('head')
  @include('layouts.landingpage.header')
@endsection
@section('title','Your Event')
@push('style')
@endpush
@section('content')
    <!--Main content-->
    <div id="root"></div>
@endsection
@push('script')
  <!-- Bootstrap core JavaScript -->

  <!-- <script src="{!!asset('frontend/js/jquery.min.js')!!}"></script> -->
  <!-- Bootstrap 4.0-->
  <script src="{!! asset('js/app.js') !!}"></script>

  <!-- <script src="{!! asset('frontend/js/bootstrap.min.js')!!}"></script>

  <script src="{!!asset ('frontend/js/bootstrap.bundle.min.js')!!}"></script> -->

  <!-- Plugin JavaScript -->
  <script src="{!!asset ('frontend/js/jquery.easing.min.js')!!}"></script>
  <script src="{!!asset ('frontend/js/scrollreveal.min.js')!!}"></script>
  <script src="{!!asset ('frontend/js/jquery.magnific-popup.min.js')!!}"></script>
  <script src="{!!asset ('frontend/js/jquery.slimscroll.min.js')!!}"></script>

  <script src="{!!asset ('frontend/js/template.min.js')!!}"></script>
  <!-- Custom scripts for this template -->
  <script src="{!!asset('frontend/js/creative.min.js')!!}"></script>

@endpush
